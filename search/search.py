'''
    File name: search.py
    Description: Search problems by tag
    Author: GDV
    Python Version: 3.6.7
    Windows compatibility: No idea. I guess?
'''


import json
from os import path
import readline


INDENT = "    "


class Completer(object):
    """Auto completes when pressing <TAB>"""

    def __init__(self, options):
        self.options = sorted(options)

    def complete(self, text, state):
        # Code found at https://stackoverflow.com/a/7821956
        if state == 0:  # on first trigger, build possible matches
            if text:  # cache matches (entries that start with entered text)
                self.matches = [s for s in self.options
                                if s and s.startswith(text)]
            else:  # no text entered, all matches possible
                self.matches = self.options[:]
        # return match indexed by state
        try:
            return self.matches[state]
        except IndexError:
            return None


def main():
    """Let the user find problems from their tags"""
    # retrieve tags
    directory = path.dirname(__file__)
    fileName = path.join(directory, "index.json")
    with open(fileName) as dataFile:
        data = json.load(dataFile)
    tags = getAllTags(data)
    # set completer
    completer = Completer(tags)
    readline.set_completer(completer.complete)
    readline.parse_and_bind('tab: complete')
    # start
    printTags(tags)
    while True:
        try:
            tag = input("Enter a tag: ")
        except KeyboardInterrupt:
            print("")
            break
        problems = findProblems(tag, data)
        printProblems(problems)


def getAllTags(data):
    """Add all the tags in an array"""
    tags = []
    for problem in data["problems"]:
        if "tags" in problem:
            for tag in problem["tags"]:
                if tag not in tags:
                    tags.append(tag)
    tags.sort()
    return tags


def printTags(tags):
    """Print all existing tags"""
    N_COLUMNS = 3
    MAX_WORD_SIZE = 18
    print("Existing tags:\n\n", end=INDENT)
    for i, tag in enumerate(sorted(tags), 1):
        if len(tag) > MAX_WORD_SIZE + 2:
            tag = tag[:MAX_WORD_SIZE] + ".."
        print(tag.ljust(MAX_WORD_SIZE + 4), end=""),
        if i % N_COLUMNS == 0:
            print("\n", end=INDENT)
    print("\n")


def findProblems(tag, data):
    """Add in an array the problems that contain the tag specified"""
    problems = []
    for problem in data["problems"]:
        if "tags" in problem:
            if tag in problem["tags"]:
                problems.append(problem)
    return problems


def printProblems(problems):
    """Print the problems specified"""
    print("")
    for problem in problems:
        print(INDENT + str(problem["id"]) + ": " + problem["name"])
        print(2 * INDENT + ', '.join(problem["tags"]))

    print("")


main()
