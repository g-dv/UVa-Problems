# UVa-Problems

Personal collection of cpp solutions to UVa problems (from https://uva.onlinejudge.org/).

## Search

The point of keeping these solutions is that they can be useful to find inspiration or to save up some time when encountering a similar problem.

Therefore, I add some simple tags for each problem in the `./search/index.json` file in order to search using the Python 3 script located in the same folder.

## Regarding comments

Since these solutions were not meant to be shared initially, some comments are written in French. This is especially true for the oldest additions.
However, variables are in English, so it should be comprehensible for English speakers.
