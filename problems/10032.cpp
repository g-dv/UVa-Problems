/**
 * Problem : UVa n°10032
 * Name    : Tug of War
 * Author  : https://gitlab.com/g-dv/
 * Date    : 22/09/2016
 */

#include <cstdio>

using namespace std;

const int MAX_PEOPLE_NUMBER = 100;
const int MAX_WEIGHT = 450;
const int MAX_WEIGHTS_SUM = MAX_PEOPLE_NUMBER * MAX_WEIGHT;

int main()
{
	
	int nTests;
	scanf("%d", &nTests);
	while(nTests--) {
        
        // Déclarations.
        int nPeople, sumWeights;
        int weights[100];
        // L'array a pour index une somme de poids <= sommeTotale / 2.
        // L'array contient des bitmasks indiquant le nombre de personnes
        // requises pour faire une équipe du poids de l'index.
        // E.g. combinations[61] = 11010 signifie qu'il est possible de faire
        //      une équipe de 61 kg avec 1, 3 ou 5 personnes. L'autre équipe
        //      sera +/= lourde et pèsera sommeTotale - 61 kg.
        long long combinations[MAX_WEIGHTS_SUM + 1];
        
        // Saisie.
        sumWeights = 0;
		scanf("%d", &nPeople);
		for(int i = 0; i < nPeople; i++) {
			scanf("%d", &weights[i]);
            sumWeights += weights[i];
		}
        
        // Initialisation.
        for(int i = 0; i <= MAX_WEIGHTS_SUM; i++)
            combinations[i] = 0;
        combinations[0] |= 1LL << 0; // 0 personne pour une équipe de poids 0
        
        // On détermine le bitmask permettant d'identifier une équipe de bonne
        // taille. E.g. 1100 pour un total de 5 personnes (équipes de 2 ou 3).
        long long requiredBitmask;
        if(nPeople % 2) { // Impair.
            requiredBitmask = 3LL << (nPeople / 2);
        } else { // Pair.
            requiredBitmask = 1LL << (nPeople / 2);
        }
        
        // Traitement.
        for(int i = 0; i < nPeople; i++)
            for(int j = (sumWeights / 2); j >= weights[i]; j--)
                // On ajoute des combinaisons possibles pour atteindre J.
                combinations[j] |= combinations[j - weights[i]] << 1LL;
		
        // On cherche une équipe pouvant être faite avec le bon nombre de
        // personnes. On commence par chercher dans les équipes ayant le poids
        // total le plus proche de la moitié de la somme totale des poids.
        int i = (sumWeights / 2);
        while(!(combinations[i] & requiredBitmask))
            i--;
        
        // Sortie.
        printf("%d %d\n", i, sumWeights - i);
        if(nTests)
            printf("\n");
	}

	return 0;
}