/**
 * Problem : UVa n°10344
 * Name    : 23 out of 5
 * Author  : https://gitlab.com/g-dv/
 * Date    : 15/09/2017
 */

#include <iostream>

using namespace std;

int mask[5];
int values[5];
bool isPossible;

/**
 * Recursive function
 * @param total : current total
 * @param depth : current step
 */
void backtrack(int total, int depth) {
    
    // exit condition
    if (depth == 5) {
        if (total == 23)
            isPossible = true;
        return;
    }
    
    // recursive calls
    for (int i = 0; i < 5; i++) {
        if(mask[i] == false) {
            mask[i] = true; // mark the newly added value as 'already used'
            backtrack(total + values[i], depth + 1);
            backtrack(total - values[i], depth + 1);
            backtrack(total * values[i], depth + 1);
            mask[i] = false;
        }
    }
}

int main() {

    while (true) {
        
        // initialization
        isPossible = false;
        for (int i = 0; i < 5; i++)
            mask[i] = false;

        // input
        for (int i = 0; i < 5; i++)
            cin >> values[i];
        
        // exit the program
        if (values[0] + values[1] + values[2] + values[3] + values[4] == 0)
            break;

        // processing
        for (int i = 0; i < 5 && !isPossible; i++) {
            mask[i] = true; // mark the value as 'already used'
            backtrack(values[i], 1);
            mask[i] = false;
        }
        
        // output
        if (isPossible)
            cout << "Possible" << endl;
        else
            cout << "Impossible" << endl;
      
    }
    
    return 0;
}

