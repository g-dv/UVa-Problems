/**
 * Problem : UVa n°562
 * Name    : Dividing coins
 * Author  : https://gitlab.com/g-dv/
 * Date    : 22/09/2016
 */

#include <cstdio>
#include <cmath>
#include <algorithm>

using namespace std;

bool memo[100][50000];
int nCoins, sumCoins;
int coins[100];

/**
 * Fonction récursive.
 * Trouve une répartition minimisant la différence de valeur.
 * @param noCoin : nombre de pièces déjà distribuées
 * @param given1 : valeur des pièces données au personnage 1.
 * @return       : différence minimale entre given1 et (sum - given1)
 */
int backtrack(int noCoin, int given1)
{
    // Toutes les pièces ont été distribuées.
    if(noCoin == nCoins)
        return abs(sumCoins - (2 * given1));
    // Situation déjà évaluée.
    if(memo[noCoin][given1] == true)
        return (1 << 30);
    
    // On enregistre la situation.
    memo[noCoin][given1] = true;
    
    // On évalue.
    return min(
        backtrack(noCoin + 1, given1),
        backtrack(noCoin + 1, given1 + coins[noCoin])
    );
}

int main()
{
    
    int nTests;
    scanf("%d", &nTests);
    while(nTests--) {
        
        // Initialisation.
        for(int i = 0; i < 100; i++)
            for(int j = 0; j < 50000; j++)
                memo[i][j] = false;
        
        // Saisie.
        sumCoins = 0;
        scanf("%d", &nCoins);
        for(int i = 0; i < nCoins; i++) {
            scanf("%d", &coins[i]);
            sumCoins += coins[i];
        }
        
        // Sortie.
        printf("%d\n", backtrack(0, 0));
    }

    return 0;
}
