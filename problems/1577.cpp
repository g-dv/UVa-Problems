/**: GDV
 * Problem : UVa n°1577
 * Name    : Low Power
 * Author  : https://gitlab.com/g-dv/
 * Date    : 15/02/2019
 */

#include <iostream>
#include <vector>
#include <algorithm>

typedef unsigned int uint;

using namespace std;

vector<int> batteries;
int n, k, nBatteries;

bool isDifferenceEnough(int authorizedDifference)
{
    // We keep track of the number of slots that can be used to store
    // a battery that would not fit (ie. that has no pair)
    int emergencySlots = 0;
    // For each battery
    for (uint i = 0; i < nBatteries; i++)
        // [i] is the lowest battery that still needs to be treated.
        // if the diff. between [i] and [i+1] is greater than the authorized
        // difference, then [i] won't match with any battery [i+j], j >= 1
        // In other words, we need to "hide" i with smaller batteries
        if (batteries[i + 1] - batteries[i] > authorizedDifference)
        {
            // we use an emergency slot to store [i]
            if (--emergencySlots < 0) // no emergency slot left
                return false;         // doesn't fit anywhere
        }
        // [i] is the lowest battery that still needs to be treated.
        // if the diff. between [i] and [i+1] respects the authorized
        // difference, then matching [i] and [i+1] together is the best
        // option, because, let's say we match [i] with [i+2], then [i+1]
        // remains, and [i+1] will be less likely to match with the next
        // batteries than [i+2] since the batteries are ordered.
        else
        {
            // We solve a machine, and so we have new emergency slots
            // 2k is the number of slots in the machine we solve. We substract
            // 2 because we use 2 slots in the machine to store [i] and [i+1]
            emergencySlots += 2 * k - 2;
            i++; // we jump [i+1] since we stored [i+1] already
        }
    return true;
}

int main()
{
    while (cin >> n)
    {
        cin >> k;
        nBatteries = 2 * n * k;
        batteries = vector<int>(nBatteries);
        for (int i = 0; i < nBatteries; i++)
            cin >> batteries[i];

        sort(batteries.begin(), batteries.end());

        // Simple dichotomy to find the root of the difference
        int lower = 0;
        int upper = batteries.back() - batteries.front();
        while (upper - lower > 1)
        {
            int middle = (lower + upper) / 2;
            if (isDifferenceEnough(middle))
                upper = middle;
            else
                lower = middle;
        }
        cout << (isDifferenceEnough(lower) ? lower : upper) << endl;
    }

    return 0;
}

// Example :
// authorizedDifference = 7
//  n = 2
//  k = 3
//
// |  |  |  |----|  |  |  |            |  |  |  |----|  |  |  |
// batteries = 1 2 3 11 19 27 35 43 44 45 46 47
//    2 - 1 <= 7 so we can happily put them in machine n°1
// |01|..|..|----|02|..|..|            |  |  |  |----|  |  |  |
// batteries =     3 11 19 27 35 43 44 45 46 47
//    11 - 3 > 7 so we can't put 3 in machine n°2 or else we're dead
//               hopefully we have a slot available in machine n°1
// |01|03|..|----|02|..|..|            |  |  |  |----|  |  |  |
// batteries =       11 19 27 35 43 44 45 46 47
//    19 - 11 > 7 so we can't put 11 in machine n°2 or else we're dead
//                hopefully we have a slot available in machine n°1
// |01|03|11|----|02|..|..|            |  |  |  |----|  |  |  |
// batteries =          19 27 35 43 44 45 46 47
//    27 - 19 > 7 so we can't put 19 in machine n°2 or else we're dead
//                hopefully we have a slot available in machine n°1
// |01|03|11|----|02|19|..|            |  |  |  |----|  |  |  |
// batteries =             27 35 43 44 45 46 47
//    35 - 27 > 7 so we can't put 27 in machine n°2 or else we're dead
//                hopefully we have a slot available in machine n°1
// |01|03|11|----|02|19|27|            |  |  |  |----|  |  |  |
// batteries =                35 43 44 45 46 47
//    43 - 35 > 7 so we can't put 35 in machine n°2 or else we're dead
//                but there is no slot left in machine n°1
// |01|03|11|----|02|19|27|            |35|  |  |----|  |  |  |
// Here, it is impossible for machine n°2 to have a difference <= 7.
// Conclusion : the minimum difference is greater than 7.
