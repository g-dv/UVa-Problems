/**
 * Problem : UVa n°10261
 * Name    : Ferry Loading
 * Author  : https://gitlab.com/g-dv/
 * Date    : 17/09/2016
 */

#include <cstdio>
#include <algorithm>
#include <utility>
#include <string>

using namespace std;

const int MAX_N_CARS = 201; // (100 m / 100 cm) * 2 sides = 200 cars.
const int MAX_FERRY_LENGTH = 10000; // (100 m / 100 cm) * 2 sides = 200 cars.

int cars[MAX_N_CARS];
bool memo[MAX_N_CARS][MAX_FERRY_LENGTH];
int nCars;
int ferryLength;

/**
 * Fonction récursive.
 * Trouve un ordre permettant de rentrer le plus de voitures possible.
 * @param noCar      : nombre de voitures rentrées sur le pont
 * @param port       : longueur de la file à bâbord
 * @param starboard  : longueur de la file à tribord
 * @return           : noCar et la chaîne de décision en ordre inverse (la
 *                     décision pour la dernière voiture est à l'index 0)
 */
pair<int, string> backtrack(int noCar, int port, int starboard)
{
    // Vérifie que la situation actuelle soit valide.
    // Si non, la désigne comme nulle (noCar = 0).
    // Vérifie ensuite que la situation n'a pas déjà été traitée.
    if(port > ferryLength || starboard > ferryLength)
        return make_pair(0, "");
    if(noCar > nCars)
        return  make_pair(0, "");
    if(memo[noCar][max(port, starboard)])
        return make_pair(0, "");
    
    // Enregistre la situation.
    memo[noCar][max(port, starboard)] = true;
    
    // Essaye en mettant la voiture suivante à babord ou à tribord.
    pair<int, string> casePort = backtrack(
        noCar + 1,
        port + cars[noCar],
        starboard
    );
    pair<int, string> caseStarboard = backtrack(
        noCar + 1,
        port,
        starboard + cars[noCar]
    );
    
    // Détermine l'option optimale.
    pair<int, string> max;
    if(casePort.first > caseStarboard.first) {
        // mettre à bâbord a permis une meilleure optimisation
        max = casePort;
        max.second.push_back('p');
    } else {
        // mettre à bâbord a permis une meilleure optimisation
        max = caseStarboard;
        max.second.push_back('s');
    }
    if(max.first > noCar) {
        return max;
    } else {
        // Les deux cas ont renvoyé 0. Il n'a pas été possible d'ajouter une
        // voiture, donc on garde le voiture.
        return make_pair(noCar, "");
    }
}

int main()
{
    int nTests;
    scanf("%d", &nTests);
    while(nTests--) {
        
        // Saisie.
        scanf("%d", &ferryLength);
        ferryLength *= 100;
        for(int i = 0; i < MAX_N_CARS; i++)
            cars[i] = 0;
        nCars = -1;
        while(scanf("%d", &cars[++nCars]), cars[nCars]);
        
        // Nettoyage du tableau memo.
        for(int i = 0; i < MAX_N_CARS; i++)
            for(int j = 0; j < MAX_FERRY_LENGTH; j++)
                memo[i][j] = false;
        
        // Traitement.
        pair<int, string> answer = backtrack(1, cars[0], 0);
        int nCars = answer.first;
        
        // Sortie.
        printf("%d\n", nCars);
        if(nCars != 0)
            printf("port\n");
        for(int i = 0; i < nCars - 1; i++) {
            if(answer.second[nCars - 2 - i] == 'p')
                printf("port\n");
            else
                printf("starboard\n");
        }
        if(nTests)
            printf("\n");
    }

    return 0;
}