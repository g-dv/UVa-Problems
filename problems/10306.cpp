/**
 * Problem : UVa n°10306
 * Name    : e-Coins
 * Author  : https://gitlab.com/g-dv/
 * Date    : 28/04/2017
 */

#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

struct Coin
{
    int conventional = 0;
    int info = 0;
};

const int MAX = 1 << 30;

int main()
{
    int nTests;
    cin >> nTests;
    while(nTests--) {
        // Saisie.
        int nTypes, modulus, squaredModulus;
        cin >> nTypes >> modulus;

        // Initialisation.
        squaredModulus = modulus * modulus; // Gain de performance.

        vector<Coin> coins(nTypes);
        for(int i = 0; i < nTypes; i++)
            cin >> coins[i].conventional >> coins[i].info;

        vector<vector<int>> minNCoins;
        vector<int> empty(modulus + 1, MAX);
        for(int i = 0; i <= modulus; i++)
            minNCoins.push_back(empty);
        minNCoins[0][0] = 0;

        // Traitement.
        // Programmation dynamique bottom-up : on trouve le nombre minimal de
        // pièces pour faire toutes les paires conventional-info.
        int result = MAX;
        for(Coin coin : coins) {
            for(int i = coin.conventional; i <= modulus; i++) {
                for(int j = coin.info; j <= modulus; j++) {
                    if(i * i + j * j > squaredModulus) // Inutile de continuer.
                        break;
                    minNCoins[i][j] = min(
                        minNCoins[i][j],
                        minNCoins[i - coin.conventional][j - coin.info] + 1
                    );
                    // Si le modulus fonctionne, on met à jour le résultat.
                    if(i * i + j * j == squaredModulus)
                        result = min(result, minNCoins[i][j]);
                }
            }
        }

        // Sortie.
        cout << (result != MAX ? to_string(result) : "not possible") << endl;
    }
    
    return 0;
}
