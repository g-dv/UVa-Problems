/**
 * Problem : UVa n°10911
 * Name    : Forming Quiz Teams
 * Author  : https://gitlab.com/g-dv/
 * Date    : 07/10/2016
 */

#include <cstdio>
#include <iostream>
#include <algorithm>
#include <string>
#include <cmath>

using namespace std;

double memo[1 << 16];
int nPairs;
double dist[16][16];

struct Coordinates {
    int x, y;
};


/**
 * Calcule la distance entre deux coordonnées.
 * @param  a  coordonnées du point a
 * @param  b  coordonnées du point b
 * @return    distance entre a et b
 */
double getDistance(Coordinates a, Coordinates b)
{
    return hypot(a.x - b.x, a.y - b.y);
}

/**
 * Fonction récursive. Détermine la somme minimale.
 * Efface les 1 du bitmask au fur et à mesure que les paires se forment.
 * Renvoie toujours la somme minimale des distances entre les "1" du bitmask.
 * Exemple : quand il ne reste que deux 1, renvoie la sitance entre les deux.
 * @param  bitmask  bitmask de (2 * nPairs) bits. Représente l'état des paires
 * @return          somme minimale entre les "1" du paramètres
 */
double backtrack(int bitmask)
{
    // Il ne reste plus de 1. La distance entre les 1 est donc nulle.
    if(bitmask == (0))
        return 0;
    
    // On a déjà trouvé la somme minimale pour les 1 restants dans le bitmask.
    if(memo[bitmask] != -1)
        return memo[bitmask];
    
    double minSum = 1000000000;
    
    // On appelle la fonction récursivement en formant en paramètre une
    // nouvelle paire. C'est-à-dire qu'on réduit le nombre de 1 dans le
    // bitmask pour trouver la somme minimale d'un plus petit échantillon (dp).
    for(int i = 0; i < 2 * nPairs; i++) {
        if(bitmask & (1 << i)) { // Ca pourrait être l'individu 1.
            for(int j = i + 1; j < 2 * nPairs; j++) {
                if(bitmask & (1 << j)) { // Ca pourrait être l'individu 2.
                    // On regarde si somme de la distance de la paire formée et
                    // de la somme minimale des paires restantes est optimale.
                    minSum = min(
                        minSum,
                        dist[i][j] + backtrack(bitmask ^ (1 << i | 1 << j))
                    );
                }
            }
        }
    }
    
    // On enregistre la somme minimale entre les "1" du paramètre.
    memo[bitmask] = minSum;
    
    return minSum;
}

int main()
{
    int noCase = 0;
    while(cin >> nPairs, nPairs) {
        
        // Saisie.
        Coordinates coord[16];
        string name;
        for(int i = 0; i < 2 * nPairs; i++) {
            cin.ignore();
            cin >> name >> coord[i].x >> coord[i].y;
        }
        
        // On crée un tableau des distances entre chaque paire possible.
        double distances[16][16];
        for(int i = 0; i < 2 * nPairs; i++)
            for(int j = i + 1; j < 2 * nPairs; j++)
                dist[i][j] = dist[j][i] = getDistance(coord[i], coord[j]);
        
        // On initialise le memo.
        fill_n(memo, 1 << 16, -1);
        
        // Sortie.
        printf("Case %d: %.2f\n", ++noCase, backtrack((1 << (2 * nPairs)) - 1));
    }

    return 0;
}