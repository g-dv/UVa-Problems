/**
 * Problem : UVa n°1260
 * Name    : Sales
 * Author  : https://gitlab.com/g-dv/
 * Date    : 06/09/2016
 */

#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int nTests;
    cin >> nTests;
    for(int i = 0; i < nTests; i++) {
        // Saisie.
        vector<int> a;
        int nDays;
        cin >> nDays;
        for(int j = 0; j < nDays; j++) {
            int input;
            cin >> input;
            a.push_back(input);
        }
        
        // Calcul de la somme.
        int sum = 0;
        for(int j = 1; j < nDays; j++) {
            for(int k = 0; k < j; k++) {
                if(a[k] <= a[j])
                    sum++;
            }
        }
        
        // Affichage.
        cout << sum << endl;
    }
    
    return 0;
}