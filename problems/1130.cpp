/**: GDV
 * Problem : UVa n°1130
 * Name    : Men at work
 * Author  : https://gitlab.com/g-dv/
 * Date    : 15/02/2019
 */

#include <iostream>
#include <algorithm>
#include <string>
#include <cmath>
#include <vector>
#include <queue>
#include <utility>

using namespace std;

typedef unsigned int uint;
typedef vector<bool> vb;
typedef vector<vb> vvb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;

/**
 * My initial approach was to create a big graph of PERIOD x N x N vertices.
 * The edges coming out of a node would point to the nodes located nearby
 * (Same node, North, South, East, West), but would be associated to the time
 * t + 1 (modulo GRID_PERIOD). GRID_PERIOD being the time before the grid is
 * back to its original state.
 * Then, I could simply use Dijkstra to compute the shortest paths to every
 * node, and I could print the minimal distance for nodes [..][N-1][N-1].
 * This solution gave the right results but I did not realize that
 * GRID_PERIOD = LCM(2, 4, 6, ..., 18) was big. Therefore, my algorithm was too
 * slow. So, I modified it so that the set of nodes and edges are not stored
 * entirely but are only generated when needed. The result is a modified
 * Dijkstra that computes the neighbors when needed using a function that tells
 * if a node is free or not at a given time. I must also use a memory to keep
 * track of the nodes I already visited.
 */

// Time after which the grid returns in the initial state.
const int GRID_PERIOD = 5040; // Least common multiple of (2, 4, ..., 18)

struct Node
{
    Node(int time, int i, int j) : time(time), i(i), j(j)
    {
    }
    int time, i, j;
};

const pair<uint, uint> MOVES[5] = {make_pair(0, 0),  // nothing
                                   make_pair(-1, 0), // north
                                   make_pair(1, 0),  // south
                                   make_pair(0, -1), // west
                                   make_pair(0, 1)}; // east

vvb initialStates; // are the roads open or not initially?
vvi periods;       // how long before they switch state?
int N;

/**
 * Tells if a road is open at a given time
 * @param time time
 * @param i row
 * @param i column
 * @return true if and only if the road is open
 */
bool isOpen(int time, int i, int j)
{
    bool initial = initialStates[i][j];
    int period = periods[i][j];
    // initial state if it is in the first half of the cycle
    if (!period)
        return initial;
    bool isInitialState = time % (2 * period) < period;
    return (isInitialState ? initial : !initial);
}

int main()
{
    while (cin >> N)
    {
        if (periods.size()) // if not first
            cout << endl;

        // Input initial grid.
        char tmp;
        initialStates = vvb(N, vb(N));
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < N; j++)
            {
                cin >> tmp;
                initialStates[i][j] = (tmp == '.' ? true : false);
            }
        }

        // Input periods.
        periods = vvi(N, vi(N));
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < N; j++)
            {
                cin >> tmp;
                periods[i][j] = tmp - '0';
            }
        }

        // We implement the classic Dijkstra algorithm, but instead of storing
        // every edge, we create them as we need them.
        queue<Node> nodes;
        nodes.push(Node(0, 0, 0));
        vvvi alreadyVisited(GRID_PERIOD, vvi(N, vi(N, false)));
        int result = -1;

        while (nodes.size() && result == -1)
        {
            // Since we don't go back in time, the front node in the FIFO is
            // necessarily one of the nodes with this minimum distance.
            Node current = nodes.front();
            // Remove the node.
            nodes.pop();

            // Inspect neighbors.
            int time = current.time + 1;
            for (uint k = 0; k < 5 && result == -1; k++)
            {
                // set [time % GRID_PERIOD][i][j] the coords. of the neighbor
                int i = current.i + MOVES[k].first;
                int j = current.j + MOVES[k].second;
                if (i >= 0 && i < N && j >= 0 && j < N)
                {
                    if (!alreadyVisited[time % GRID_PERIOD][i][j])
                    {
                        if (isOpen(time, i, j))
                        {
                            // The node is a neighbor.
                            // If the node hasn't been visited yet, since we
                            // can't go back in time, and since we treat nodes
                            // with a FIFO, we know for a fact that we found a
                            // shortest path to reach this node.
                            if (i == N - 1 && j == N - 1)
                                // We finally reached the exit!
                                result = time;
                            nodes.push(Node(time, i, j));
                            // Make sure we won't visit it again (similar to
                            // removing it from the list in a classic Dijkstra
                            // implementation).
                            alreadyVisited[time % GRID_PERIOD][i][j] = true;
                        }
                    }
                }
            }
        }
        // We could not find any path to the destination.
        cout << (result != -1 ? to_string(result) : "NO") << endl;
    }

    return 0;
}
