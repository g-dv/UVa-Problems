/**
 * Problem : UVa n°10123
 * Name    : No Tipping
 * Author  : https://gitlab.com/g-dv/
 * Date    : 30/09/2016
 */

#include <cstdio>
#include <vector>

using namespace std;

int boardLength, boardWeight, nPackages, noTest = 1, nCentered;
int positions[20], weights[20], torques[20];
bool memo[1 << 20];

/**
 * Détermine si le plateau bascule ou non
 * @param  bitmask  bitmask les paquets présents sur le plateau
 * @return  bool indiquant si le plateau bascule ou non
 */
bool isTipping(int bitmask)
{
    double torque = 0, weight = boardWeight;
    for(int i = 0; i < nPackages; i++) {
        if(bitmask & (1 << i)) {
            weight += weights[i];
            torque += torques[i];
        }
    }
    torque /= weight;
    if(torque >= 1.5 || torque <= -1.5)
        return true;
    return false;
}

/**
 * Détermine si le plateau bascule ou non.
 * @param  nAdded  nombre de paquets sur le plateau
 * @param  bitmask  bitmask les paquets présents sur le plateau
 * @return  order  vecteur contenant le numéro des paquets dans le bon ordre
 */
vector<int> backtrack(int nAdded, int bitmask)
{
    vector<int> order;
    
    if(memo[bitmask])
        return order;
    
    memo[bitmask] = true;
    
    if(isTipping(bitmask))
        return order;
    
    if(nAdded == nPackages) {
        
        order.push_back(99); // On met 99 pour signifier qu'on a trouvé.
        return order;
    }
    
    // On ignore les nCentered premiers éléments (déjà placés car centrés).
    for(int i = nCentered; i < nPackages; i++) {
        order = backtrack(nAdded + 1, bitmask | (1 << i));
        if(order.size() != 0) { // On a trouvé une solution.
            if(order[0] == 99)    // On enlève le marqueur avant
                order.pop_back(); // de mettre le dernier paquet.
            order.push_back(i);
            return order;
        }
    }
}

/**
 * Échange les positions de deux éléments.
 * @param  a  index du premier élément 
 * @param  b  index du second élément
 */
void swap(int a, int b)
{
    int tmp = positions[a];
    positions[a] = positions[b];
    positions[b] = tmp;
    tmp = weights[a];
    weights[a] = weights[b];
    weights[b] = tmp;
    tmp = torques[a];
    torques[a] = torques[b];
    torques[b] = tmp;
}

int main()
{
    while(scanf("%d %d %d", &boardLength, &boardWeight, &nPackages),
        (boardLength || boardWeight || nPackages))
    {
        // Saisie.
        for(int i = 0; i < nPackages; i++) {
            scanf("%d %d", &positions[i], &weights[i]);
            torques[i] = positions[i] * weights[i];
        }
        
        // Initialisation.
        for(int i = 0; i < (1 << 20); i++)
            memo[i] = false;
        
        // Pour gagner du temps, on ajoute directement les paquets au centre.
        nCentered = 0;
        int bitmask = 0;
        for(int i = 0; i < nPackages; i++) {
            if(positions[i] >= -1 && positions[i] <= 1) {
                swap(i, nCentered++); // Regroupe les paquets centrés au début.
                bitmask = bitmask * 2 + 1;
            }
        }
        
        // On ignorera les nCentered premiers éléments car ils sont centrés.
        vector<int> order = backtrack(nCentered, bitmask);
        
        // Sortie.
        printf("Case %d:\n", noTest++);
        if(order.size() == 0)
            printf("Impossible\n");
        else {
            // On affiche l'ordre qu'on a trouvé pour ceux décentrés.
            for(int i = 0; i < nPackages - nCentered; i++) {
                int j = order[i];
                printf("%d %d\n", positions[j], weights[j]);
            }
            // On affiche les derniers à retirer. Ceux du centre.
            for(int i = 0; i < nCentered; i++) {
                printf("%d %d\n", positions[i], weights[i]);
            }
        }
    }
    
    return 0;
}