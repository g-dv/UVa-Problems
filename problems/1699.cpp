/**
 * Problem : UVa n°1699
 * Name    : Crane Balancing
 * Author  : https://gitlab.com/g-dv/
 * Date    : 11/05/2019
 */

#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

typedef unsigned int uint;

const double INF = 1e10;
const double EPSILON = 1e-10;

struct Point
{
    double x = 0.0;
    double y = 0.0;
};

double getArea(const vector<Point> &points)
{
    double area = 0.0;
    for (uint i = 0; i < points.size() - 1; i++)
        area += points[i].x * points[i + 1].y - points[i].y * points[i + 1].x;
    return (area / 2.0);
}

double getCenterOfMassX(const vector<Point> &points, double area)
{
    // apply the formula
    double centerX = 0.0;
    for (uint i = 0; i < points.size() - 1; i++)
    {
        double x1 = points[i].x, x2 = points[i + 1].x;
        double y1 = points[i].y, y2 = points[i + 1].y;
        centerX += (x1 + x2) * (x1 * y2 - y1 * x2) / (6.0 * area);
    }
    return centerX;
}

int main()
{
    uint nPoints;
    while (cin >> nPoints)
    {
        // input
        double left = INF, right = -INF;
        vector<Point> points(nPoints);
        for (uint i = 0; i < nPoints; i++)
        {
            cin >> points[i].x >> points[i].y;
            if (!points[i].y)
            {
                left = min(left, points[i].x);
                right = max(right, points[i].x);
            }
        }
        points.push_back(points[0]);

        // calculate area and center of gravity (x is enough)
        double area = abs(getArea(points));
        double center = getCenterOfMassX(points, getArea(points));
        double x = points[0].x;

        // if the crane is facing left, use a symmetric one
        if (x < center) // weight is added on the left side
        {
            double tmp = left;
            left = -right, right = -tmp, x = -x, center = -center;
        }

        // output
        if (center > right || left >= x)
            cout << "unstable" << '\n';
        else
        {
            // solve the equation (positive torque = negative torque)
            uint lowerValue = int(0 + EPSILON + area * (left - center) / (x - left));
            uint upperValue = int(1 - EPSILON + area * (right - center) / (x - right));
            cout << ((left <= center) ? 0 : lowerValue) << " .. "
                 << ((x <= right) ? "inf" : to_string(upperValue)) << endl;
        }
    }

    return 0;
}
