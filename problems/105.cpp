/**
 * Problem : UVa n°105
 * Name    : The Skyline Problem
 * Author  : https://gitlab.com/g-dv/
 * Date    : 25/04/2017
 */

#include <iostream>
#include <vector>

using namespace std;

int main() {
	
	vector<int> city;

	// Saisie et traitement.
	int left, height, right;
	while (cin >> left >> height >> right) {
		for (int i = left; i < right; i++) {
			while (i >= city.size())
				city.push_back(0);
			if (height > city[i])
				city[i] = height;
		}
	}

	// Sortie.
	height = 0;
	for (int i = 0; i < city.size(); i++) {
		if (height != city[i]) {
			height = city[i];
			cout << i << " " << height << " ";
		}
	}
	cout << city.size() << " " << 0 << endl;

	return 0;
}
