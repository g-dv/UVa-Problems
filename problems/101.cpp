/**
 * Problem : UVa n°101
 * Name    : The Blocks Problem
 * Author  : https://gitlab.com/g-dv/
 * Date    : 27/09/2016
 */

#include <iostream>
#include <vector>
#include <utility>
#include <string>

using namespace std;

typedef pair<unsigned int, unsigned int> uu;

vector<vector<int>> blocks;

/**
 * Cherche le numéro de pile et la position dans la pile d'un bloc.
 * @param x        : n° du bloc cherché
 * @return .first  : numéro de la pile contenant le bloc
 * @return .second : position du bloc dans la pile
 */
uu find(int x)
{
    for(unsigned int i = 0; i < blocks.size(); i++)
        for(unsigned int j = 0; j < blocks[i].size(); j++)
            if(blocks[i][j] == x)
                return make_pair(i, j);
}

/**
 * Ajoute le bloc a dans la pile contenant le bloc b
 * @param parameter : string indiquant le type d'ajout ("over" ou "onto")
 * @param a : n° du bloc à prélever.
 * @param b : n° du bloc sur lequel on va ajouter le bloc a.
 */
void move(string parameter, int a, int b)
{
    // position des éléments.
    uu aPos = find(a), bPos = find(b);
    // piles concernées.
    vector<int> *aPile = &blocks[aPos.first], *bPile = &blocks[bPos.first];
    
    if((aPos.first != bPos.first) && (a != b)) { // Opération autorisée.
    
        // Enlève ce qui est au-dessus de a.
        for(auto it = (aPile->begin() + aPos.second + 1); it < aPile->end(); it++)
            blocks[*it].push_back(*it);
        aPile->erase(aPile->begin() + aPos.second, aPile->end());
        
        if(parameter == "onto") {
            // On enlève ce qui est au-dessus de b.
            for(auto it = (bPile->begin() + bPos.second + 1); it < bPile->end(); it++)
                blocks[*it].push_back(*it);
            bPile->erase(bPile->begin() + bPos.second + 1, bPile->end());
        }
        
        // On place a sur b.
        bPile->push_back(a);
    }
}

/**
 * Ajoute la pile contenant le bloc a sur celle contenant le bloc b
 * @param parameter : string indiquant le type d'empilement ("over" ou "onto")
 * @param a : n° du bloc à partir du quel on prélève les blocs à empiler.
 * @param b : n° du bloc sur lequel on va empiler.
 */
void pile(string parameter, int a, int b)
{
    // position des éléments.
    uu aPos = find(a), bPos = find(b);
    // piles concernées.
    vector<int> *aPile = &blocks[aPos.first], *bPile = &blocks[bPos.first];
    
    if((aPos.first != bPos.first) && (a != b)) { // Opération autorisée.
    
        if(parameter == "onto") {
            // On enlève ce qui est au-dessus de b.
            for(auto it = (bPile->begin() + bPos.second + 1); it < bPile->end(); it++)
                blocks[*it].push_back(*it);
            bPile->erase(bPile->begin() + bPos.second + 1, bPile->end());
        }
        
        // On déplace la pile de a sur celle de b.
        bPile->insert(bPile->end(), aPile->begin() + aPos.second, aPile->end());
        aPile->erase(aPile->begin() + aPos.second, aPile->end());
    }
}

int main() {
    
    // Saisie.
    int nBlocks;
    cin >> nBlocks;
    
    for(int i = 0; i < nBlocks; i++) {
        blocks.push_back(vector<int>(1, i));
    }
    
    // Traitement des ppérations.
    string operation, parameter;
    int source, destination;
    cin >> operation;
    while(operation != "quit") {
        cin >> source >> parameter >> destination;
        cin.ignore();
        if(operation == "move")
            move(parameter, source, destination);
        else if(operation == "pile")
            pile(parameter, source, destination);
        cin >> operation;
    }
    
    // Affichage.
    for(int i = 0; i < nBlocks; i++) {
        cout << i << ":";
        for(unsigned int j = 0; j < blocks[i].size(); j++)
            cout << " " << blocks[i][j];
        cout << endl;
    }
    
    return 0;
}