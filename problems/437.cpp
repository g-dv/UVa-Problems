/**
 * Problem : UVa n°437
 * Name    : The Tower of Babylon
 * Author  : https://gitlab.com/g-dv/
 * Date    : 28/02/2019
 */

#include <cstdio>
#include <vector>
#include <algorithm>

using namespace std;

typedef unsigned int uint;

// I belive this problem was supposed to be solved with
// top-down dynamic programming, but I remembered that I had already
// solved a similar problem at school with this trick of sorting
// the blocks by area.

struct Block
{
    Block(uint width, uint length, uint height)
        : _width(width),
          _length(length),
          _height(height),
          _maxHeight(height) {}
    uint _width, _length, _height;
    // max height for the tower consisting only of blocks with
    // an area bigger or equal to this one's
    uint _maxHeight;
    uint area() const { return _width * _length; }
    bool fitsOn(const Block &block) const
    {
        return _width < block._width && _length < block._length;
    }
    bool operator<(const Block &block) const { return area() < block.area(); }
};

int main()
{
    uint nBlocks;
    uint noTest = 0;
    scanf("%u", &nBlocks);
    while (nBlocks)
    {
        vector<Block> blocks;
        for (uint i = 0; i < nBlocks; i++)
        {
            // Input.
            vector<uint> xyz(3);
            scanf("%u %u %u", &xyz[0], &xyz[1], &xyz[2]);
            sort(xyz.begin(), xyz.end());
            // Add the 3 relevant orientations.
            blocks.push_back(Block(xyz[0], xyz[1], xyz[2]));
            blocks.push_back(Block(xyz[0], xyz[2], xyz[1]));
            blocks.push_back(Block(xyz[1], xyz[2], xyz[0]));
        }
        // Sort the blocks by increasing area.
        sort(blocks.begin(), blocks.end());

        uint maxTowerHeight = 0;
        for (uint i = 0; i < blocks.size(); i++)
        {
            Block &current = blocks[blocks.size() - i - 1];
            for (uint j = 0; j < i; j++) // all the blocks that are bigger
            {
                Block &previous = blocks[blocks.size() - j - 1];
                if (current.fitsOn(previous))
                {
                    // if "current" can go on top of "previous"...
                    // ...update the maxHeight.
                    uint newMaxHeight = previous._maxHeight + current._height;
                    current._maxHeight = max(current._maxHeight, newMaxHeight);
                }
            }
            maxTowerHeight = max(maxTowerHeight, current._maxHeight);
        }
        printf("Case %u: maximum height = %u\n", ++noTest, maxTowerHeight);
        scanf("%u", &nBlocks);
    }

    return 0;
}
