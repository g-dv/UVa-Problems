/**: GDV
 * Problem : UVa n°12207
 * Name    : That is Your Queue
 * Author  : https://gitlab.com/g-dv/
 * Date    : 12/02/2019
 */

#include <iostream>
#include <list>
#include <cstdio>
#include <algorithm>

typedef unsigned int uint;

using namespace std;

int main()
{
    uint P, C;
    uint noCase = 1;
    cin >> P >> C;
    while (P != 0)
    {
        printf("Case %u:\n", noCase++);
        // list is nice because erase is fast and we don't need the [] operator
        list<uint> queue;
        // we don't need to keep track of the order too far in the queue since
        // there can be at most 1000 commands.
        for (uint i = 1; i <= min(P, C); i++)
            queue.push_back(i);
        for (uint i = 0; i < C; i++)
        {
            char command;
            cin >> command;
            if (command == 'N')
            {
                cout << queue.front() << endl;
                if (C >= P) // useless to keep track of its position otherwise
                    queue.push_back(queue.front());
                queue.pop_front();
            }
            else
            {
                uint citizenID;
                cin >> citizenID;
                // We could complete our algorithm with a second data structure
                // to record the positions in order to avoid using remove().
                queue.erase(remove(queue.begin(), queue.end(), citizenID), queue.end());
                queue.push_front(citizenID);
            }
        }
        cin >> P >> C;
    }

    return 0;
}
