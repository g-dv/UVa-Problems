/**
 * Problem : UVa n°10576
 * Name    : Y2K Accounting Bug
 * Author  : https://gitlab.com/g-dv/
 * Date    : 26/04/2017
 */

#include <iostream>
#include <vector>

using namespace std;

int maxSum, surplus, deficits;

/**
 * Détermine la somme maximale si tous les rapports sont négatifs.
 * @param  results  résultat pour chaque mois
 */
void backtrack(vector<int> results)
{
    // On vérifie que la configuration soit valide.
    // ie. Tous les rapports montrent des déficits.
    int fiveLastResults = 0;
    for(int i = 0; i < results.size(); i++) {
        fiveLastResults += results[i];
        if(i > 4)
            fiveLastResults -= results[i - 5]; // Seulement les 5 derniers.
        if(i >= 4 && fiveLastResults > 0)
            return;
    }

    // Si la configuraiton est complète, on évalue le total.
    if(results.size() == 12) {
        int sum = 0;
        for(int result : results)
            sum += result;
        if(sum > maxSum) // Nouvelle meilleure valeur.
            maxSum = sum;
        return;
    }

    // Sinon, on poursuit la récursion.
    results.push_back(surplus);
    backtrack(results);
    results[results.size() - 1] = deficits;
    backtrack(results);
}
 
int main()
{
    while(cin >> surplus >> deficits) {
        
        // Initialisation.
        deficits = -deficits;
        maxSum = -1;
        vector<int> results;
        
        // Traitement.
        backtrack(results);
        
        // Saisie.
        cout << (maxSum < 0 ? "Deficit" : to_string(maxSum)) << endl;
    }

    return 0;
}
