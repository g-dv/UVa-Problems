/**
 * Problem : UVa n°732
 * Name    : Anagrams by Stack
 * Author  : https://gitlab.com/g-dv/
 * Date    : 26/04/2017
 */

#include <string>
#include <vector>
#include <iostream>

using namespace std;

vector<string> solutions;

string source;
string target;

/**
 * Génère les anagrammes et remplit le tableau de solutions.
 * @param  solution  tableau de "i" et "o" que l'on remplit
 * @param  result  mot formé par les opérations de solution
 * @param  stack  lettres encore pushed dans la stack
 * @param  sourceIndex  prochaine lettre du mot source à push dans la stack
 */
void backtrack(string solution, string result, string stack, int sourceIndex)
{
	if(result.size() == target.size()) // On a rentré toutes les lettres.
		solutions.push_back(solution);

	if (solution.size() == 2 * target.size())
		return;

	// On essaye 'i'.
	char letter = source[sourceIndex];
	backtrack(solution + 'i', result, stack + letter, sourceIndex + 1);

	// On essaye 'o'.
	if (stack.size() > 0) {
		result += stack.back();
		stack.pop_back();
		if(result.back() != target[result.size() - 1])
			return; // Le résultat ne diffère du mot cible.
		backtrack(solution + 'o', result, stack, sourceIndex);
	}
}

int main()
{	
	while(cin >> source >> target) {
		// Initialisation.
		solutions.clear();
		
		// Traitement.
		backtrack("", "", "", 0);

		// Sortie.
		cout << "[";
		for(const string& solution : solutions)
			for(int i = 0; i < solution.size(); i++)
				cout << (i ? " " : "\n") << solution[i];
		cout << endl << "]" << endl;
	}

	return 0;
}
