/**
 * Problem : UVa n°299
 * Name    : Train Swapping
 * Author  : https://gitlab.com/g-dv/
 * Date    : 23/04/2017
 */

#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int nTests;
    cin >> nTests;
    while (nTests--) {

        // Saisie.
        int nCarriages;
        cin >> nCarriages;
        vector<int> carriages(nCarriages);
        for (int i = 0; i < nCarriages; i++)
            cin >> carriages[i];
        
        // Traitement. Bubblesort adapté.
        int swapCount = 0;
        bool isFinished;
        do {
            isFinished = true;
            for (int i = 0; i < nCarriages - 1; i++)
            {
                if (carriages[i] > carriages[i + 1])
                {
                    int tmp = carriages[i];
                    carriages[i] = carriages[i + 1];
                    carriages[i + 1] = tmp;
                    isFinished = false;
                    swapCount++;
                }
            }
        } while (!isFinished); // Pas encore de tour à vide.
        cout << "Optimal train swapping takes " << swapCount << " swaps." << endl;
    }

    return 0;
}