/**
 * Problem : UVa n°590
 * Name    : Always on the run
 * Author  : https://gitlab.com/g-dv/
 * Date    : 26/02/2019
 */

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

typedef unsigned int uint;
typedef vector<uint> vu;
typedef vector<vu> vvu;
typedef vector<vvu> vvvu;
const uint INF = 10000000;

vvvu prices;

/**
 * Returns the price of a flight
 * @param  from  city of departure
 * @param  to  city of arrival
 * @param  day  day of departure
 * @return price of the flight
 */
uint getPrice(uint from, uint to, uint day)
{
    uint period = prices[from][to].size();
    return prices[from][to][day % period];
}

int main()
{
    uint nCities, nDays, period, noScenario = 0;
    cin >> nCities >> nDays;
    while (nCities)
    {
        // Input.
        prices = vvvu(nCities, vvu(nCities));
        for (uint i = 0; i < nCities; i++)
            for (uint j = 0; j < nCities; j++)
                if (i != j)
                {
                    cin >> period;
                    prices[i][j] = vu(period);
                    for (uint k = 0; k < period; k++)
                    {
                        cin >> prices[i][j][k];
                        if (!prices[i][j][k])
                            prices[i][j][k] = INF;
                    }
                }
                else
                    prices[i][j] = vu(1, INF);

        // Dynamic programming.
        // We compute the optimal price to land on city j on day i
        vvu costs(nDays + 1, vu(nCities, INF));
        costs[0][0] = 0;
        for (uint i = 1; i <= nDays; i++)          // on day...
            for (uint j = 0; j < nCities; j++)     // we arrive to...
                for (uint k = 0; k < nCities; k++) // coming from...
                    costs[i][j] = min(costs[i][j],
                                      costs[i - 1][k] + getPrice(k, j, i - 1));

        // Output.
        printf("Scenario #%u\n", ++noScenario);
        uint solution = costs[nDays][nCities - 1];
        if (solution != INF)
            printf("The best flight costs %u.\n\n", solution);
        else
            printf("No flight possible.\n\n");

        cin >> nCities >> nDays;
    }

    return 0;
}
