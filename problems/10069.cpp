/**
 * Problem : UVa n°10069
 * Name    : Distinct Subsequences
 * Author  : https://gitlab.com/g-dv/
 * Date    : 26/02/2019
 */

#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

typedef unsigned int uint;

// The only annoying thing with this problem is that we need to
// manipulate huge numbers.

/**
 * Represents a huge integer
 * Adapted from a code found on StackOverflow.
 */
struct BigUInt
{
    string value_;

    BigUInt() {}
    BigUInt(string value) { value_ = value; }
    BigUInt(uint value) { value_ = to_string(value); }

    BigUInt operator+=(BigUInt other)
    {
        (*this) = (*this) + other;
        return *this;
    }

    BigUInt operator+(BigUInt other) // the only operator we need...
    {
        string a = value_;
        string b = other.value_;
        reverse(a.begin(), a.end());
        reverse(b.begin(), b.end());
        BigUInt result;
        for (uint i = 0, carry = 0; i < a.size() || i < b.size() || carry; i++)
        {
            carry += (i < a.size() ? a[i] - 48 : 0);
            carry += (i < b.size() ? b[i] - 48 : 0);
            result.value_ += carry % 10 + 48;
            carry /= 10;
        }
        reverse(result.value_.begin(), result.value_.end());
        return result;
    }
};

int main()
{
    uint nTests;
    cin >> nTests;
    while (nTests--)
    {
        // Input.
        string sequence, subsequence;
        cin >> sequence >> subsequence;

        // Bottom-up dynamic programming.
        // nOccurences[i][j] = occurrences of subsequence[:i] in sequence[:j]
        vector<BigUInt> emptyLine(sequence.size() + 1, BigUInt(0));
        vector<vector<BigUInt>> nOccurences(subsequence.size() + 1, emptyLine);
        for (uint i = 0; i < nOccurences[0].size(); i++)
            nOccurences[0][i] = BigUInt(to_string(1));
        for (uint i = 1; i < nOccurences.size(); i++)
            for (uint j = 1; j < nOccurences[i].size(); j++)
            {
                nOccurences[i][j] = nOccurences[i][j - 1];
                if (subsequence[i - 1] == sequence[j - 1])
                    nOccurences[i][j] += nOccurences[i - 1][j - 1];
            }

        // Output.
        cout << nOccurences.back().back().value_ << endl;
    }

    return 0;
}