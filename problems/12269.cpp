/**: GDV
 * Problem : UVa n°12269
 * Name    : Lawn mower
 * Author  : https://gitlab.com/g-dv/
 * Date    : 18/02/2019
 */

#include <iostream>
#include <algorithm>
#include <vector>
#include <string>

using namespace std;

typedef unsigned int uint;

int main()
{
    const double FIELD_SIZE[2] = {75.0, 100.0};
    int nPasses[2]; // number of times the mower passes for x and in y
    double width;
    cin >> nPasses[0] >> nPasses[1] >> width;
    while (width)
    {
        bool isValid = true;
        for (int i = 0; i < 2; i++) // x then y
        {
            vector<double> origins(nPasses[i] + 2);
            for (int j = 0; j < nPasses[i]; j++)
                cin >> origins[j];
            origins.push_back(-width / 2.0);
            origins.push_back(width / 2.0 + FIELD_SIZE[i]);
            sort(origins.begin(), origins.end());
            for (int j = 1; j < origins.size() && isValid; j++)
                if (origins[j - 1] + width < origins[j])
                    isValid = false;
        }
        cout << (isValid ? "YES" : "NO") << endl;
        cin >> nPasses[0] >> nPasses[1] >> width;
    }

    return 0;
}
