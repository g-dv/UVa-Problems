/**
 * Problem : UVa n°116
 * Name    : Unidirectional TSP
 * Author  : https://gitlab.com/g-dv/
 * Date    : 28/02/2019
 */

#include <iostream>
#include <cstdio>
#include <vector>
#include <climits>
#include <algorithm>

using namespace std;

typedef unsigned int uint;

struct Path
{
    vector<uint> _rows;
    int _length = INT_MAX;

    bool operator<(const Path &path) const
    {
        if (_length < path._length)
            return true;
        if (_length == path._length)
            return _rows < path._rows;
        return false;
    }
};

int main()
{
    uint nRows, nColumns;
    while (cin >> nRows >> nColumns)
    {
        // Initialization.
        vector<vector<Path>> paths(nRows, vector<Path>(nColumns));
        for (uint i = 0; i < nRows; i++)
            paths[i][0]._rows = vector<uint>(1, i);

        // Input.
        for (uint i = 0; i < nRows; i++)
            for (uint j = 0; j < nColumns; j++)
                cin >> paths[i][j]._length;

        // Dynamic programming.
        for (uint noColumn = 1; noColumn < nColumns; noColumn++)
        {
            for (uint noRow = 0; noRow < nRows; noRow++)
            {
                // "is it better to come from the same row, the
                // next row or the previous row?"
                uint candidateRows[3] = {(noRow + nRows - 1) % nRows,
                                         noRow,
                                         (noRow + 1) % nRows};
                Path path;
                for (uint i = 0; i < 3; i++)
                    path = min(path, paths[candidateRows[i]][noColumn - 1]);

                path._length += paths[noRow][noColumn]._length;
                path._rows.push_back(noRow);
                paths[noRow][noColumn] = path;
            }
        }

        // Find the best path on the last column.
        Path path;
        for (uint i = 0; i < nRows; i++)
            path = min(path, paths[i][nColumns - 1]);

        // Output.
        for (uint i = 0; i < nColumns; i++)
            cout << (i ? " " : "") << path._rows[i] + 1;
        printf("\n%d\n", path._length);
    }

    return 0;
}
