/**
 * Problem : UVa n°108
 * Name    : Maximum Sum
 * Author  : https://gitlab.com/g-dv/
 * Date    : 07/10/2016
 */

#include <cstdio>
#include <algorithm>

using namespace std;

/**
 * Trouve la somme maximale d'un vecteur.
 * Version simplifiée de l'algorithme de Kadane.
 * @param  array  array à étudier
 * @param  n      nombre d'éléments à considérer dans l'array
 * @return  maxSum  somme maximale des sous-vecteurs
 */
int getMaxSum(int array[], int n)
{
    int maxSum = 0, currentSum = 0;
    for(int i = 0 ; i < n; i++) {
        currentSum += array[i];
        if(currentSum > maxSum)
            maxSum = currentSum;
        if(currentSum < 0)
            currentSum = 0;
    }
    
    return maxSum;
}

int main()
{
    int n, matrix[100][100];
    while(scanf("%d", &n) == 1) {
        
        // Saisie.
        int maxSum = -127;
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < n; j++) {
                scanf("%d", &matrix[i][j]);
                maxSum = max(maxSum, matrix[i][j]);
            }
        }
        
        // S'il n'y a aucun nombre positif, on connaît directement le résultat.
        if(maxSum > 0) {
            // On fait toutes les fusions de lignes possibles (O(N²)) et à
            // chaque fois on trouve la somme maximum du sous-vecteur (O(N)).
            for(int startIndex = 0; startIndex < n; startIndex++) {
                int array[100] = { 0 };
                for(int i = startIndex; i < n; i++) {
                    for(int j = 0; j < n; j++)
                        array[j] += matrix[i][j];
                    maxSum = max(maxSum, getMaxSum(array, n));
                }
            }
        }
        printf("%d\n", maxSum);
    }
    
    return 0;
}