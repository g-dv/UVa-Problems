/**
 * Problem : UVa n°11456
 * Name    : Trainsorting
 * Author  : https://gitlab.com/g-dv/
 * Date    : 04/05/2017
 */

#include <vector>
#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    int nTests;
    cin >> nTests;

    while (nTests--) {

        // Input.
        int nCars;
        cin >> nCars;

        vector<int> cars(nCars);
        for (int i = 0; i < nCars; i++)
            cin >> cars[i];

        // Init.
        int answer = 0;
        vector<int> lis(nCars, 0);
        vector<int> lds(nCars, 0);

        // Processing. Method: Dynamic programming - LIS.
        for (int i = nCars - 1; i >= 0; i--) {
            for (int j = nCars - 1; j >= i + 1; j--) {
                if (cars[j] < cars[i] && lis[j] > lis[i]) // LIS
                    lis[i] = lis[j];
                if (cars[j] > cars[i] && lds[j] > lds[i]) // LDS
                    lds[i] = lds[j];
            }
            // Answer is the addition of the lis and the lds (if we count the
            // i-th car only once). So, we add the current event to the lis/lds
            // and update the answer.
            answer = max(answer, (++lis[i]) + (++lds[i]) - 1);
        }

        // Output.
        cout << answer << endl;

    }

    return 0;
}
