/**
 * Problem : UVa n°10114
 * Name    : Loansome Car Buyer
 * Author  : https://gitlab.com/g-dv/
 * Date    : 24/04/2017
 */

#include <iostream>
#include <deque>

using namespace std;

int main()
{
    while (true) {

        // Saisie.
        double duration, downPayment, loanAmount, nRecords;
        cin >> duration >> downPayment >> loanAmount >> nRecords;

        if (duration < 0)
            break;

        deque<double> recordDates(nRecords);
        deque<double> recordValues(nRecords);
        for (double i = 0; i < nRecords; i += 1)
            cin >> recordDates[i] >> recordValues[i];

        // Traitement.
        double depreciation;
        double carValue = loanAmount + downPayment;
        double amountDue = loanAmount;

        for (double i = 0; i <= duration; i += 1) {

            if (i == recordDates[0]) { // Taux de dépréciation suivant.
                depreciation = recordValues[0];
                recordValues.pop_front();
                recordDates.pop_front();
            }

            carValue *= (1 - depreciation); // Nouvelle valeur de la voiture...

            if (i > 0)
                amountDue -= (loanAmount / duration); // ... et de la dette.
            
            // Sortie.
            if (amountDue < carValue) { // La condition est remplie.
                cout << i << " month";
                if (i != 1)
                    cout << "s";
                cout << endl;
                break;
            }

        }
    }

    return 0;
}