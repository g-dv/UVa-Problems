/**
 * Problem : UVa n°102
 * Name    : Ecological Bin Packing
 * Author  : https://gitlab.com/g-dv/
 * Date    : 04/09/2016
 */

#include <cstdio>
#include <string>
#include <algorithm>

using namespace std;

int main()
{
    while(true) {
        // Déclarations.
        int movements, minMovements = 1000000000;
        string bestOrder;
        char order[3] = {'B', 'C', 'G'}; // Ordre alphabétique (next_permutation).
        int boxes[3][3];
        
        // Entrées.
        bool hasEnded = false;
        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++) {
                hasEnded = (scanf("%d", &boxes[i][j]) == EOF);
                if(hasEnded)
                    return 0;
            }
        }
        
        // Somme totale.
        int sum = 0;
        for(int i = 0; i < 3; i++)
            for(int j = 0; j < 3; j++)
                sum += boxes[i][j];

        // Recherche pour les 3! = 6 configurations.
        do {
            movements = sum;
            for(int i = 0; i < 3; i++) {
                // Pour chaque boîte, on soustrait du nombre total de bouteilles
                // à déplacer celles qui sont déjà dans la bonne boîte.
                switch(order[i]) {
                case 'B':
                    movements -= boxes[i][0];
                    break;
                case 'C':
                    movements -= boxes[i][2];
                    break;
                case 'G':
                    movements -= boxes[i][1];
                }
            }
            if(movements < minMovements) {
                minMovements = movements;
                bestOrder = string(order, order + 3);
            }
        } while (next_permutation(order, order + 3));
        
        // Sortie.
        printf("%s %d\n", bestOrder.c_str(), minMovements);
    }
    
    return 0;
}