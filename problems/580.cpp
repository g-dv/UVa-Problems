/**
 * Problem : UVa n°580
 * Name    : Critical Mass
 * Author  : https://gitlab.com/g-dv/
 * Date    : 26/02/2019
 */

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

typedef unsigned int uint;
typedef vector<uint> vu;
typedef vector<vu> vvu;

int main()
{
    // Input.
    vu stackSizes;
    while (true)
    {
        uint stackSize;
        cin >> stackSize;
        if (!stackSize)
            break;
        stackSizes.push_back(stackSize);
    }

    // We can solve every input at once.
    uint maxStackSize = *max_element(stackSizes.begin(), stackSizes.end());

    // Dynamic programming.
    // nValid[5][0] = number of valid stacks with ****L
    // nValid[5][1] = number of valid stacks with ***LU
    // nValid[5][2] = number of valid stacks with ***UU
    vvu nValid(maxStackSize + 2, vu(3, 0));
    nValid[0][0] = 1;

    for (uint i = 1; i <= maxStackSize + 1; i++)
    {
        // add a L on top of either *L, LU or UU
        nValid[i][0] = nValid[i - 1][0] + nValid[i - 1][1] + nValid[i - 1][2];
        // add a U on top of a *L
        nValid[i][1] = nValid[i - 1][0];
        // add a U on top of a LU
        nValid[i][2] = nValid[i - 1][1];
    }

    // Output
    for (uint stackSize : stackSizes)
        // nInvalid = nTotal - nValid = 2^N - sum(nValid[N])
        cout << ((1 << stackSize) - nValid[stackSize + 1][0]) << endl;

    return 0;
}
