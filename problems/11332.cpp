/**
 * Problem : UVa n°11332
 * Name    : Summing Digits
 * Author  : https://gitlab.com/g-dv/
 * Date    : 14/09/2017
 */

#include <iostream>

using namespace std;

int main() {
    int input;
    while (cin >> input && input) { // input
        int sum;
        do { // sum the digits
            sum = 0;
            while (input) {
                sum += input % 10;
                input /= 10;
            }
            input = sum;
        } while (input >= 10); // until we reach the answer
        // output
        cout << sum << endl;
    }

    return 0;
}
