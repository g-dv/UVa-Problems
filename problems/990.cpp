/**
 * Problem : UVa n°990
 * Name    : Diving for Gold
 * Author  : https://gitlab.com/g-dv/
 * Date    : 23/09/2016
 */

#include <cstdio>
#include <algorithm>
#include <cmath>
#include <vector>

using namespace std;

const int MAX_TIME = 1000;
const int MAX_TREASURE_NUMBER = 30;

int main()
{
    int availableTime, speed, nTreasures;
    bool firstSet = true;
    while(scanf("%d %d %d", &availableTime, &speed, &nTreasures) > 0) {
        
        if(!firstSet)
            printf("\n");
        firstSet = false;
        
        // Saisie des trésors.
        int depths[MAX_TREASURE_NUMBER];
        int values[MAX_TREASURE_NUMBER];
        
        for(int i = 0; i < nTreasures; i++) {
            scanf("%d %d", &depths[i], &values[i]);
        }
    
        // Initialisation.
        // Valeur max pour [noTreasure][availableTime].
        int optimalValues[MAX_TREASURE_NUMBER + 1][MAX_TIME + 1];
        // Pistes requise pour durée max.
        vector<int> optimalTreasures[MAX_TREASURE_NUMBER + 1][MAX_TIME + 1];
        
        // On remplit la première colonne (lorsque l'on a 0 seconde d'air).
        for(int i = 0; i < nTreasures; i++) {
            optimalValues[i][0] = 0;
        }
        
        // On remplit la première ligne (lorsqu'on a étudié aucun trésor).
        for(int i = 0; i < availableTime; i++) {
            optimalValues[0][i] = 0;
        }
        
        // On remplit le reste du tableau.
        for(int i = 1; i <= nTreasures; i++) {
            int time = 3 * speed * depths[i - 1];
            int value = values[i - 1];
            for(int j = 1; j <= availableTime; j++) {
                // Si la piste est trop longue ou il serait plus optimisé de ne pas la prendre.
                if((j < time) || (optimalValues[i - 1][j] > (value + optimalValues[i - 1][abs(j - time)]))) {
                    optimalValues[i][j] = optimalValues[i - 1][j];
                    optimalTreasures[i][j].insert(optimalTreasures[i][j].end(), optimalTreasures[i - 1][j].begin(), optimalTreasures[i - 1][j].end());
                } else { // On prend la piste et on optimise l'espace restant.
                    optimalValues[i][j] = value + optimalValues[i - 1][abs(j - time)];
                    optimalTreasures[i][j].insert(optimalTreasures[i][j].end(), optimalTreasures[i - 1][abs(j - time)].begin(), optimalTreasures[i - 1][abs(j - time)].end());
                    optimalTreasures[i][j].push_back(i - 1);
                }
            }
        }
        
        // Sortie.
        int optimalNumber = (int)(optimalTreasures[nTreasures][availableTime].size());
        printf("%d\n%d\n", optimalValues[nTreasures][availableTime], optimalNumber);
        for(int i = 0; i < optimalNumber; i++) {
            int noTreasure = optimalTreasures[nTreasures][availableTime][i];
            printf("%d %d\n", depths[noTreasure], values[noTreasure]);
        }
    }

    return 0;
}
