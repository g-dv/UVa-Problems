/**
 * Problem : UVa n°165
 * Name    : Stamps
 * Author  : https://gitlab.com/g-dv/
 * Date    : 14/10/2016
 */

#include <cstdio>
#include <vector>
#include <algorithm>

using namespace std;

/**
 * NB : Il n'y a que 1+2+3+4+5+6+7+8 = 36 entrées possibles. Il serait donc tout
 * à fait possible de pré-calculer le résultat pour chaque entrée possible afin
 * que le programme s'exécute instantanément.
 */

int h, k, approxMaxStampValue, maxValue = 0;
vector<int> bestCombination;

/**
 * Trouve la valeur atteignable maximale pour une combinaison donnée.
 * @param combination combinaison de valeurs des timbres
 * @return valeur maximale atteignable
 */
int findAttainableValue(vector<int> combination)
{
    // Initialisation
    int dp[141]; // max(h * combination[i]) = (5 * 28) = 140
    fill_n(dp, 141, 9); // on remplit avec des valeurs quelconques > 8.
    dp[0] = 0;
    
    // Remplissage du tableau (bottom-up dynamic programming).
    for(unsigned int i = 0; i < combination.size(); i++) // pour chaque timbre
        for(int j = combination[i]; j <= h * combination[i]; j++)
            dp[j] = min(dp[j], 1 + dp[j - combination[i]]);
    
    // On parcourt le tableau pour trouver la valeur maximale atteignable.
    int value = 1;
    while(dp[++value] <= h);
    
    return value - 1;
}

/**
 * Fonction récursive. Génère des combinaisons et les teste.
 * Si le vecteur représentant la combinaison est complet, la teste. Sinon,
 * on ajoute un élément dans le vecteur.
 * @param combination vecteur contenant les valeurs des timbres
 * @param nStamps nombre d'éléments déjà présents dans le vecteur
 */
void testCombinations(vector<int> combination, int nStamps)
{
    if(nStamps == k) { // Le vecteur est plein. On teste.
        int value = findAttainableValue(combination);
        if(value > maxValue) { // C'est la meilleure trouvée.
            bestCombination = combination;
            maxValue = value;
        }
    } else { // On doit ajouter des valeurs dans la combinaison.
        // On teste en ajoutant toutes les valeurs possibles. On ajoute pas de
        // valeur supérieure à "approxMaxStampValue" car ça serait inutile.
        for(int i = combination[nStamps - 1] + 1; i <= approxMaxStampValue + nStamps; i++) {
            combination.push_back(i);
            testCombinations(combination, nStamps + 1);
            combination.pop_back();
        }
    }
}

int main() {
    
    while(true) {
        
        // Saisie.
        scanf("%d %d", &h, &k);
        if(!(h || k))
            break;
        
        // Initialisation.
        maxValue = 0;
        bestCombination.clear();
        bestCombination.push_back(1);
        
        // Traitement.
        if(k == 1) { // Cas facile. Gain de temps.
            maxValue = h;
        } else if(h == 1) { // Cas facile. Gain de temps.
            for(int i = 2; i <= k; i++)
                bestCombination.push_back(i);
            maxValue = k;
        } else { // Cas difficile. On génère des combinaisons et on les teste.
            // On calule grossièrement la valeur limite d'un timbre à ajouter.
            // Cette fonction donne des résultats satisfaisants.
            approxMaxStampValue =
                (2 * h * (k - 1)) // environ proportionnel à h * k
                - ((max(k, h) - min(k, h)) - 1) // malus si abs(h-k) est grand
                - 4; // ajustement pratique
            testCombinations(bestCombination, 1);
        }

        // Affichage.
        for(unsigned int i = 0; i < bestCombination.size(); i++)
            printf("%3d", bestCombination[i]);
        printf(" ->%3d\n", maxValue);
    }
    
    return 0;
}