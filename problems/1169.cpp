/**: GDV
 * Problem : UVa n°1169
 * Name    : Robotruck
 * Author  : https://gitlab.com/g-dv/
 * Date    : 14/02/2019
 */

#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>
#include <climits>
#include <utility>

using namespace std;

typedef unsigned int uint;
struct Location
{
    int x, y;
};

vector<Location> coords;

/**
 * Computes the distance between the destination of two packages
 * @param i : index of the first package
 * @param j : index of the second package
 * @return : distance between the points
 */
int getMoves(int i, int j)
{
    return abs(coords[i].x - coords[j].x) + abs(coords[i].y - coords[j].y);
}

int main()
{
    uint nTests;
    cin >> nTests;

    while (nTests--)
    {
        uint nPackages, capacity;
        cin >> capacity >> nPackages;

        vector<int> weights(nPackages + 1);
        coords = vector<Location>(nPackages + 1);
        coords[0].x = coords[0].y = 0;
        for (uint i = 1; i <= nPackages; i++)
            cin >> coords[i].x >> coords[i].y >> weights[i];

        // minMoves[i] is the minimum number of moves to deliver package i
        vector<int> minMoves(nPackages + 1, INT_MAX);
        minMoves[0] = 0;

        for (int i = 1; i <= nPackages; i++)
        {
            // while it is possible to go from j to i without reloading
            int load = weights[i];
            for (int j = i; load <= capacity && j; load += weights[--j])
            {
                // we go to j - 1 as efficiently as possible
                int candidate = minMoves[j - 1];
                // then we go back to (0, 0) before heading to j
                candidate += getMoves(j - 1, 0) + getMoves(0, j);
                // then we go from j to i without going back to base
                for (int k = j; k < i; k++)
                    candidate += getMoves(k, k + 1);
                // is this path better?
                minMoves[i] = min(minMoves[i], candidate);
            }
        }
        int result = minMoves.back() + getMoves(nPackages, 0);
        cout << result << (nTests ? "\n\n" : "\n");
    }

    return 0;
}
