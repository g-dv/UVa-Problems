/**
 * Problem : UVa n°628
 * Name    : Passwords
 * Author  : https://gitlab.com/g-dv/
 * Date    : 15/09/2017
 */

#include <iostream>
#include <vector>

using namespace std;

vector<string> words;
string rule;

/**
 * Recursive function. Creates and displays all the possible passwords
 * @param depth : step of the recursion, index of rule
 * @return password : generated string
 */
void backtrack(int depth, string password) {
    
    // password found
    if (depth == rule.length()) {
        cout << password << endl;
        return;
    }
    
    // add a digit
    if (rule[depth] == '0')
        for (int i = 0; i < 10; i++)
            backtrack(depth + 1, password + to_string(i));

    // add a word
    else
        for (int i = 0; i < words.size(); i++)
            backtrack(depth + 1, password + words[i]);
}

int main() {
    int nWords;
    while (cin >> nWords) {
        cout << "--" << endl;

        // input words
        words.resize(nWords);
        for (int i = 0; i < nWords; i++)
            cin >> words[i];

        // input number of rules
        int nRules;
        cin >> nRules;

        // solve
        for (int i = 0; i < nRules; i++) {
            cin >> rule;
            backtrack(0, "");
        }
    }
    
    return 0;
}
