/**
 * Problem : UVa n°750
 * Name    : 8 Queens Chess Problem
 * Author  : https://gitlab.com/g-dv/
 * Date    : 04/09/2016
 */

#include <cstdio>
#include <string>

using namespace std;

int inputI, inputJ, cols[8], nSolutions;

/*
 * Vérifie qu'un emplacement soit valide.
 * @return  bool indiquant si l'emplacement est valide.
 */
bool isValid(int i, int j)
{
    // Déjà une reine sur la colonne.
    if(cols[j] != 99) {
        return false;
    }
    for(int k = 0; k < 8; k++) {
        // Déjà une reine sur la ligne.
        if(cols[k] == i) {
            return false;
        }
        // Déjà une reine sur la diagonale.
        if(abs(k - j) == abs(cols[k] - i)) {
            return false;
        }
    }
    return true;
}

/**
 * Cherche un emplacement valide pour une colonne.
 * Passe à la colonne suivante si un emplacement est trouvé (récursif).
 * On retourne à la colonne précédente si la routine prend fin.
 * @param  j  int indiquant l'index de la colonne (0 à 7).
 */
void resolveColumn(int j)
{
    // Si on a déjà posé les 8 dames
    if(j == 8) {
        // On affiche la solution trouvée.
        printf("%2d     ", ++nSolutions);
        for(int i = 0; i < 8; i++)
            printf(" %d", cols[i] + 1);
        printf("\n");
    }
    // Si on est sur la colonne entrée par l'utilisateur on passe directement à la suivante.
    if((inputJ - 1) == j) {
        resolveColumn(j + 1);
    } else {
        // Pour chaque ligne de la colonne.
        for(int i = 0; i < 8; i++) {
            if(isValid(i, j)) {
                // L'emplacement est valide.
                // On met une dame et on passe à la colonne suivante.
                cols[j] = i;
                resolveColumn(j + 1);
                // On retire la dame à la sortie de la récursion et on continue de descendre.
                cols[j] = 99;
            }
        }
    }
}

int main()
{
    int nTests;
    scanf("%d", &nTests);
    while(nTests--) {
        nSolutions = 0;
        for(int i = 0; i < 8; i++)
            cols[i] = 99;
        scanf("%d %d", &inputI, &inputJ);
        // On place la dame initiale.
        cols[inputJ - 1] = inputI - 1;
        printf("SOLN       COLUMN\n");
        printf(" #      1 2 3 4 5 6 7 8\n\n");
        resolveColumn(0);
        if(nTests != 0)
            printf("\n");
    }
    
    return 0;
}