/**
 * Problem : UVa n°111
 * Name    : History Grading
 * Author  : https://gitlab.com/g-dv/
 * Date    : 04/05/2017
 */

#include <vector>
#include <cmath>
#include <iostream>
#include <string>

using namespace std;

int main()
{
    // Input (1st line)
    int nEvents;
    cin >> nEvents;

    // Input (2nd line)
    vector<int> events(nEvents);
    for (int i = 0; i < nEvents; i++)
        cin >> events[i];

    int eventPosition;
    while (cin >> eventPosition) {

        // Input (3rd+ lines)
        vector<int> answer(nEvents); // student's proposed answer
        answer[eventPosition - 1] = 0;
        for (int i = 1; i < nEvents; i++) {
            cin >> eventPosition;
            answer[eventPosition - 1] = i;
        }

        // Processing. Method: Dynamic programming - LIS.
        vector<int> lis(nEvents, 0);
        lis[0] = 1;
        int maxScore = 1;
        for (int i = 1; i < nEvents; i++) {
            for (int j = 0; j < i; j++)
                if (events[answer[j]] < events[answer[i]] && lis[j] > lis[i])
                    lis[i] = lis[j];
            // add the current event to the lis and update the best score
            maxScore = max(maxScore, ++lis[i]);
        }

        // Output.
        cout << maxScore << endl;
    }

    return 0;
}

