/**
 * Problem : UVa n°147
 * Name    : Dollars
 * Author  : https://gitlab.com/g-dv/
 * Date    : 01/03/2019
 */

#include <cstdio>
#include <vector>
#include <cmath>

using namespace std;

typedef unsigned int uint;

const uint MAX_AMOUNT = 30001;

int main()
{
    // Initialize coins (in cents).
    vector<uint> coins{10000, 5000, 2000, 1000, 500, 200, 100, 50, 20, 10, 5};

    // Dynamic programming (coin change)
    vector<long long> nWays(MAX_AMOUNT, 0);
    nWays[0] = 1;
    for (uint i = 0; i < coins.size(); i++)
        for (uint j = coins[i]; j < MAX_AMOUNT; j++)
            nWays[j] += nWays[j - coins[i]];

    // Input-Output
    double amount;
    scanf("%lf", &amount);
    while (amount != 0.0)
    {
        uint index = round(amount * 100.0);
        printf("%6.2f%17llu\n", amount, nWays[index]);
        scanf("%lf", &amount);
    }

    return 0;
}
