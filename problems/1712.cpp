/**: GDV
 * Problem : UVa n°1712
 * Name    : Cutting Cheese
 * Author  : https://gitlab.com/g-dv/
 * Date    : 18/02/2019
 */

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <vector>
#include <string>
#include <cmath>

using namespace std;

typedef unsigned int uint;

struct Hole
{
    int x, y, z, r;
};

const double CHEESE_SIZE = 100000.0;

vector<Hole> holes;

/**
 * Returns the volume of the cheese located between z=0 and z=parameter
 * @param z thickness of the cut
 * @return volume
 */
double getVolume(double z)
{
    double volume = z * pow(CHEESE_SIZE, 2.0);
    for (auto hole : holes) // we must substract the holes
    {
        // to integrate, we want a system where:
        //     0 is the z coordinate of the lowest point of the hole
        //     a is the limit behind which we integrate
        double relativeZ = z - hole.z + hole.r;
        double a = min(2.0 * hole.r, max(0.0, relativeZ));
        // Let radiusOfTheCut(x) = sqrt(hole.r² - (x - hole.r)²)
        // Let areaOfTheCut(x) = radiusOfTheCut(x)² * pi
        // Then, the volume of the hole is:
        //     integral of areaForThisCut(x) for x from 0 to a
        // The result is:
        double holeVolume = (1.0 / 3.0) * M_PI * a * a * (3 * hole.r - a);
        volume -= holeVolume;
    }
    return volume;
}

int main()
{
    int nHoles, nSlices;
    while (cin >> nHoles >> nSlices)
    {
        double totalVolume = pow(CHEESE_SIZE, 3);
        holes = vector<Hole>(nHoles);
        for (Hole &hole : holes)
        {
            cin >> hole.r >> hole.x >> hole.y >> hole.z;
            totalVolume -= (M_PI * 4.0 / 3.0) * pow(hole.r, 3);
        }
        double sliceVolume = totalVolume / nSlices;

        double previousZ = 0;
        for (int i = 0; i < nSlices; i++)
        {
            // Dichotomy to find the value.
            double lower = previousZ;
            double upper = CHEESE_SIZE;
            double middle;
            while (upper - lower > 0.000001)
            {
                middle = (upper + lower) / 2.0;
                if (getVolume(middle) - getVolume(previousZ) < sliceVolume)
                    lower = middle;
                else
                    upper = middle;
            }
            middle = (upper + lower) / 2.0;
            double thickness = (middle - previousZ) / 1000.0;
            cout << setprecision(11) << thickness << endl;
            previousZ = middle;
        }
    }

    return 0;
}