/**
 * Problem : UVa n°624
 * Name    : CD
 * Author  : https://gitlab.com/g-dv/
 * Date    : 22/09/2016
 */

#include <cstdio>
#include <algorithm>
#include <vector>
#include <cmath>

using namespace std;

const int MAX_LENGTH = 10000;
const int MAX_PIST_NUMBER = 20;

int main()
{
    int length; // N dans l'énoncé.
    int nTracks;
    
    while(scanf("%d %d", &length, &nTracks) > 0) {
        
        // Saisie des pistes.
        int tracks[MAX_PIST_NUMBER];
        for(int i = 0; i < nTracks; i++) {
            scanf("%d", &tracks[i]);
        }
    
        // Initialisation.
        // Durée max pour [noPiste][cassette].
        int optimalLength[MAX_PIST_NUMBER + 1][MAX_LENGTH + 1];
        // Pistes requise pour durée max.
        vector<int> optimalTracks[MAX_PIST_NUMBER + 1][MAX_LENGTH + 1];
        
        // On remplit la première colonne (lorsque N = 0).
        for(int i = 0; i < nTracks; i++) {
            optimalLength[i][0] = 0;
        }
        
        // On remplit la première ligne (lorsqu'il n'y a aucune piste).
        for(int i = 0; i < length; i++) {
            optimalLength[0][i] = 0;
        }
        
        // On remplit le reste du tableau.
        for(int i = 1; i <= nTracks; i++) {
            for(int j = 1; j <= length; j++) {
                int track = tracks[i - 1]; // durée de la piste étudiée.
                // Si la piste est trop longue ou il serait plus optimisé de ne pas la prendre.
                if((j < track) || (optimalLength[i - 1][j] > (track + optimalLength[i - 1][abs(j - track)]))) {
                    optimalLength[i][j] = optimalLength[i - 1][j];
                    optimalTracks[i][j].insert(optimalTracks[i][j].end(), optimalTracks[i - 1][j].begin(), optimalTracks[i - 1][j].end());
                } else { // On prend la piste et on optimise l'espace restant.
                    optimalLength[i][j] = track + optimalLength[i - 1][abs(j - track)];
                    optimalTracks[i][j].insert(optimalTracks[i][j].end(), optimalTracks[i - 1][abs(j - track)].begin(), optimalTracks[i - 1][abs(j - track)].end());
                    optimalTracks[i][j].push_back(i - 1);
                }
            }
        }
        
        // Sortie.
        for(unsigned int i = 0; i < optimalTracks[nTracks][length].size(); i++)
            printf("%d ", tracks[optimalTracks[nTracks][length][i]]);
        printf("sum:%d\n", optimalLength[nTracks][length]);
    }

    return 0;
}
