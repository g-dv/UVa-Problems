/**
 * Problem : UVa n°315
 * Name    : Network
 * Author  : https://gitlab.com/g-dv/
 * Date    : 03/12/2016
 */

#include <iostream>
#include <climits>
#include <vector>
#include <algorithm>
#include <set>
#include <utility>
#include <string>
#include <sstream>

using namespace std;

/**
 * Représente un arc orienté vers un noeud.
 */
struct Edge
{
    int dest, length;
    Edge():length(1){}
};

/**
 * Applique l'algorithme de Dijkstra et indique si
 * un des noeuds non-bloqués est inatteignable.
 * @param  graph        graphe sur lequel effectuer le test
 * @param  blockedNode  noeud bloqué
 * @return true is un noeud non-bloqué est inatteignable - false sinon
 */
int isUnreachable(const vector<vector<Edge>>& graph, int blockedNode) {
    int source = (blockedNode + 1) % graph.size();
    // Dijkstra : initialisation.
    vector<int> minDistances(graph.size(), INT_MAX);
    minDistances[source] = 0;
    set<pair<int, int>> activeVertices;
    activeVertices.insert({0, source});
    // Dijkstra : traitement.
    while (!activeVertices.empty()) {
        int vertice = activeVertices.begin()->second;
        activeVertices.erase(activeVertices.begin());
        for (Edge edge : graph[vertice]) {
            if (minDistances[edge.dest] > minDistances[vertice] + edge.length) {
                activeVertices.erase({minDistances[edge.dest], edge.dest});
                minDistances[edge.dest] = minDistances[vertice] + edge.length;
                activeVertices.insert({minDistances[edge.dest], edge.dest});
            }
        }
    }
    // On ignore la distance pour atteindre le noeud bloqué.
    minDistances[blockedNode] = 0;
    // On retourne true si un noeud n'est pas atteignable.
    return *max_element(minDistances.begin(), minDistances.end()) >= 10000000;
}

int main() {
    
    int nEdges;
    while(cin >> nEdges && nEdges) {
        
        // Initialisation.
        vector<vector<Edge>> graph(nEdges);
        Edge toNode;
        
        // Saisie du graphe.
        while(cin >> toNode.dest, toNode.dest) {
            
            // Le noeud minimum (1) devient 0 etc.
            toNode.dest--;
            
            // On récupère la ligne.
            string line;
            getline(cin, line);
            stringstream stream(line);
            
            // On lit la ligne.
            Edge fromNode;
            while(stream >> fromNode.dest) {
                fromNode.dest--;
                graph[toNode.dest].push_back(fromNode);
                graph[fromNode.dest].push_back(toNode);
            }
            
        }
        
        int nCriticalNodes = 0;
        for(int i = 0; i < nEdges; i++) {
            
            // On bloque le noeud i en augmentant la taille de tous ses arcs.
            for(int j = 0; j < nEdges; j++) {
                for(int k = 0; k < (int)graph[j].size(); k++) {
                    // Si l'arc relie le noeud bloqué...
                    if(j == i || graph[j][k].dest == i)
                        graph[j][k].length = 10000000;
                    else // On libère les arcs précédemment bloqués.
                       graph[j][k].length = 1; 
                }
            }
            
            // On regarde s'il est possible d'accéder à tous les noeuds.
            if(isUnreachable(graph, i))
                nCriticalNodes++;
            
        }
        
        cout << nCriticalNodes << endl;
    }

    return 0;
}
