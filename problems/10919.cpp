/**
 * Problem : UVa n°10919
 * Name    : Prerequisites?
 * Author  : https://gitlab.com/g-dv/
 * Date    : 29/04/2017
 */

#include <vector>
#include <algorithm>
#include <iostream>

using namespace std;

int main()
{
    int nTakenCourses, nCategories;
    while (cin >> nTakenCourses >> nCategories) {

        vector<int> taken(nTakenCourses);
        for (int i = 0; i < nTakenCourses; i++)
            cin >> taken[i];

        bool willGraduate = true;
        while (nCategories--) {
            int nAvailable, nRequired;
            cin >> nAvailable >> nRequired;

            while (nAvailable--) {
                int course;
                cin >> course;
                
                if (find(taken.begin(), taken.end(), course) != taken.end())
                    nRequired--;
            }
            if (nRequired > 0) // at least one course is missing
                willGraduate = false;
        }

        cout << (willGraduate ? "yes" : "no") << endl;
    }

    return 0;
}
