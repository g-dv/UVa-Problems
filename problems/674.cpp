/**
 * Problem : UVa n°674
 * Name    : Coin Change
 * Author  : https://gitlab.com/g-dv/
 * Date    : 07/10/2016
 */

#include <cstdio>
#include <algorithm>

using namespace std;

int main()
{
    // 7489 est la plus grande valeur telle que le résultat rentre dans un int.
    const int MAX_AMOUNT = 7489;
    const int COINS[5] = { 1, 5, 10, 25, 50 };
    
    unsigned int nSolutions[MAX_AMOUNT + 1];
    fill_n(nSolutions, MAX_AMOUNT + 1, 1); // On initialise à 1.
        
    for(int i = 1; i < 5; i++) // On a déjà rempli pour i = 0.
        for(int j = COINS[i]; j <= MAX_AMOUNT; j++)
            nSolutions[j] += nSolutions[j - COINS[i]];

    
    // Le tableau est rempli pour toutes les valeurs d'un coup comme ça le
    // temps d'exécution n'est pas proportionnel au nombre de tests.
    int amount;
    while(scanf("%d", &amount) > 0)
        printf("%d\n", nSolutions[amount]);

    return 0;
}