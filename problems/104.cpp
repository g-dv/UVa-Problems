/**
 * Problem : UVa n°104
 * Name    : Arbitrage
 * Author  : https://gitlab.com/g-dv/
 * Date    : 27/02/2019
 */

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

typedef unsigned int uint;
typedef vector<uint> vu;
typedef vector<vu> vvu;
typedef vector<double> vd;
typedef vector<vd> vvd;
const uint INF = 10000;

int main()
{
    uint nCountries;
    while (cin >> nCountries)
    {
        // Input.
        vvd rates(nCountries, vd(nCountries, 1.0));
        for (uint i = 0; i < nCountries; i++)
            for (uint j = 0; j < nCountries; j++)
                if (i != j)
                    cin >> rates[i][j];

        // Dynamic programming similar to problem n°590 except
        // this time we need to:
        //  - check for every starting currency
        //  - remember the path
        // ie. profits[i][j] contains the best sequence...
        //  - ...of size i
        //  - ...starting from "start"
        //  - ...ending on j
        uint minLength = INF;
        string solution = "no arbitrage sequence exists";
        for (uint start = 0; start < nCountries; start++)
        {
            vvd profits(nCountries + 1, vd(nCountries, 0.0));
            vvu previous(nCountries + 1, vu(nCountries));
            profits[0][start] = 1.0;
            for (uint i = 1; i < profits.size(); i++)
            {
                for (uint j = 0; j < profits[i].size(); j++)
                {
                    for (uint k = 0; k < profits[i - 1].size(); k++)
                    {
                        double profit = rates[k][j] * profits[i - 1][k];
                        if (profit > profits[i][j])
                        {
                            profits[i][j] = profit;
                            previous[i][j] = k;
                        }
                    }
                }
                if (profits[i][start] > 1.01 && i < minLength)
                {
                    // reconstruct the path
                    solution = to_string(start + 1);
                    uint last = start;
                    for (uint k = i; k != 0; k--)
                    {
                        last = previous[k][last];
                        solution = to_string(last + 1) + " " + solution;
                    }
                    // update the best solution
                    minLength = i + 1;
                    break;
                }
            }
        }
        cout << solution << endl;
    }

    return 0;
}
