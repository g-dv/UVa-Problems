/**
 * Problem : UVa n°608
 * Name    : Counterfeit Dollar
 * Author  : https://gitlab.com/g-dv/
 * Date    : 14/09/2017
 */

#include <iostream>
#include <vector>
#include <string>

using namespace std;

/*
 * Erase a letter from a string.
 * @param str string
 * @param letter to erase
 */
void erase(string &str, char letter) {
    unsigned int position = str.find(letter);
    if (position != -1)
        str.erase(str.begin() + position);
}

/*
 * Checks if a letter is in a string.
 * @param str string to test
 * @param letter potentially in str
 * @return bool that tells if the string contains the letter
 */
bool isInString(string str, char letter) {
    return str.find(letter) != -1;
}

int main()
{
    int nTests;
    cin >> nTests;
    while (nTests--) {
    
        // initialization
        string suspects = "ABCDEFGHIJKL";
        string left, right, result;
        string heavy, light;
        
        for (int i = 0; i < 3; i++) { // for each weighing
        
            // input
            cin >> left >> right >> result;

            // if it's even, the coins on the balance can't be counterfeited
            if (result == "even") {
                for (int i = 0; i < left.size(); i++) {
                    erase(suspects, left[i]);
                    erase(suspects, right[i]);
                }
            }
            
            // if it's not even...
            else {
                // ...the counterfeited coin has to be on the balance
                string temp = suspects;
                for (char letter : temp)
                    if (!isInString(left + right, letter))
                        erase(suspects, letter);
                
                // classify potentially heavy and light coins
                if (result == "up") {
                    heavy += right;
                    light += left;
                } else {
                    heavy += left;
                    light += right;
                }
            }
        }
        
        // if a coin has been up and down, then it can't be counterfeited
        string temp = suspects;
        for (char letter : temp)
            if (isInString(heavy, letter) && isInString(light, letter))
                erase(suspects, letter);

        // output
        cout << suspects << " is the counterfeit coin and it is "
            << (isInString(heavy, suspects[0]) ? "light." : "heavy.")
            << endl;
    }

    return 0;
}

