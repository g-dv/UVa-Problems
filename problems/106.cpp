/**
 * Problem : UVa n°106
 * Name    : Fermat vs. Pythagoras
 * Author  : https://gitlab.com/g-dv/
 * Date    : 12/10/2016
 */
 
#include <cstdio>
#include <vector>
#include <cmath>

using namespace std;

/**
 * Indique si deux entiers sont premiers entre eux.
 * Applique l'algorithme d'Euclide de façon récursive.
 * @param x premier entier
 * @param y deuxième entier
 * @return true si les entiers sont premiers entre eux, false sinon
 */
bool areCoprime(int x, int y)
{
    int gcd;
    if(y == 0)
        gcd = x;
    else
        gcd = areCoprime(y, x % y);
    return (gcd == 1);
}

int main()
{
    /*
     * NB : Les performances pourraient être grandement améliorées en utilisant
     * la programmation dynamique. En effet, on pourrait calculer seulement pour
     * le plus grand N entré car le calcul passerait par tous les autres N.
     * Comme mon programme tel qu'il est a passé largement la limite de temps,
     * je n'ai pas jugé utile d'effectuer l'amélioration.
     */
    int N;
    while(scanf("%d", &N) == 1) {
        
        // Registre des nombres utilisés.
        vector<bool> foundNumbers(N + 1, false);
        
        // Initialisation.
        int nTriples = 0, nUnused = 0;
        int qMax = (sqrt((2 * N) - 1) - 1) / 2; // cf. calcul plus bas
        
        // La méthode est détaillée plus bas.
        for(int q = 1; q <= qMax; q++) {
            for(int p = q + 1; true; p++) {
                int z = (p * p) + (q * q);
                if(z > N) // m est trop grand. On passe au n suivant.
                    break;
                // On vérifie si les conditions sont respectées.
                // (test de parité et d'inter-primalité)
                if(areCoprime(q, p) && ((p - q) % 2 == 1)) {
                    nTriples++;
                    int x = (p * p) - (q * q);
                    int y = 2 * (p * q);
                    // On note les nombre utilisés pour ce triplet et pour les
                    // triplets similaires non premiers entre eux.
                    for(int k = 1; z * k <= N; k++) {
                        foundNumbers[x * k] = true;
                        foundNumbers[y * k] = true;
                        foundNumbers[z * k] = true;
                    }
                }
            }
        }
        
        // On compte le nombre de nombres non-utilisés.
        for(int i = 1; i <= N; i++)
            if(foundNumbers[i] == false)
                nUnused++;
            
        // Sortie.
        printf("%d %d\n", nTriples, nUnused);
    }
    
    return 0;
}

/*
 * MÉTHODE MATHÉMATIQUE DE RÉSOLUTION
 * 
 * On utilise les triplets pythagoriciens.
 *
 * Wikipédia :
 *     En arithmétique, un triplet pythagoricien est un triplet (x, y, z)
 *     d'entiers naturels non nuls vérifiant la relation de Pythagore :
 *     x² + y² = z².
 *     (i)  (x, y, z) est un triplet pythagoricien
 *     (ii) il existe deux entiers p et q 
 *            - naturels non nuls
 *            - de parité différente
 *            - premiers entre eux
 *            - p > q
 *          tels que :
 *            - x = p² - q²
 *            - y = 2pq
 *            - z = p² + q²
 *     (i) <=> (ii)
 *
 * Donc, pour chaque valeur de p et q comprises dans l'intervalle, il suffit de
 * trouver si les conditions sont respectées. Si oui alors on trouve directement
 * le triplet x, y, z.
 *
 * CALCUL DE LA VALEUR MAXIMALE DE q
 *
 * (h1) : q < p avec q, p entiers positifs non nuls.
 * (h2) : n² + m² <= N
 * (1)  : On cherche la valeur maximale de q telle que (h2) est recpectée.
 * (2)  : On utilise donc p = q + 1.
 * (3)  : On résout donc : q² + (q + 1)² <= N
 * (4)  : 2n² + 2n + 1 - N <= 0
 * (5)  : Delta = 1 + 2(N - 1) = 2N - 1
 * (6)  : On obtient finalement : q <= (sqrt(2N - 1) - 1) / 2
 * C'est donc inutile de continuer la recherche pour q > (sqrt(2N - 1) - 1) / 2.
 */