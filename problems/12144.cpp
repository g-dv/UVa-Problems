/**: GDV
 * Problem : UVa n°12144
 * Name    : Almost Shortest Path
 * Author  : https://gitlab.com/g-dv/
 * Date    : 15/02/2019
 */

#include <iostream>
#include <vector>
#include <utility>
#include <climits>
#include <algorithm>
#include <functional>

using namespace std;
using namespace placeholders;

typedef unsigned int uint;
typedef vector<vector<int>> Paths;
typedef vector<int> Distances;
int nNodes, nEdges;

const int MAX_LENGTH = 10000000;

struct Edge
{
    int source;
    int destination;
    int length;
};

/**
 * Uses Dijkstra to compute the shortest paths from source to position.
 * @param edges  edges of the graph
 * @return  a pair containing: 1. the minimum length to reach node [i]
 *                             2. the nodes before node [i] in the best paths
 */
pair<Distances, Paths> getBestPaths(const vector<Edge> &edges, int source)
{
    // Initialize the two vectors.
    Distances distance(nNodes, MAX_LENGTH);
    Paths previous(nNodes);
    distance[source] = 0;

    // List the nodes.
    vector<int> nodes;
    for (int i = 0; i < nNodes; i++)
        nodes.push_back(i);

    while (nodes.size())
    {
        // Choose the node i with the minimum distance[i].
        int minDistance = MAX_LENGTH;
        int current;
        for (int node : nodes)
            if (distance[node] <= minDistance)
            {
                current = node;
                minDistance = distance[node];
            }

        // Remove the node.
        nodes.erase(remove(nodes.begin(), nodes.end(), current), nodes.end());

        // List neighbors.
        vector<Edge> neighbors;
        for (const Edge &edge : edges)
            if (edge.source == current)
                neighbors.push_back(edge);

        // Update the distance to neighbors.
        for (const Edge &edge : neighbors)
        {
            int newDistance = distance[current] + edge.length;
            if (newDistance < distance[edge.destination])
            {
                distance[edge.destination] = newDistance;
                previous[edge.destination] = vector<int>(1, current);
            }
            else if (newDistance == distance[edge.destination])
                previous[edge.destination].push_back(current);
        }
    }
    return make_pair(distance, previous);
}

/**
 * Compares the origin and the destination.
 * @param edge  edge to compare
 * @param source  origin
 * @param destination  destination
 * @return  true if equal
 */
bool special_compare(const Edge &edge, int source, int destination)
{
    return (edge.source == source &&
            edge.destination == destination);
}

/**
 * Removes the edges that form the path to destination.
 * @param edges  edges of the graph
 * @param previousNodes  antecendents of the nodes
 * @param dest  node pointed by the paths we need to remove
 */
void removeEdges(vector<Edge> &edges, const Paths &previousNodes, int dest)
{
    for (int previousNode : previousNodes[dest])
    {
        auto functor = bind(special_compare, _1, previousNode, dest);
        auto edgesToRemove = remove_if(edges.begin(), edges.end(), functor);
        edges.erase(edgesToRemove, edges.end());
        removeEdges(edges, previousNodes, previousNode);
    }
}

int main()
{
    int source, destination;
    cin >> nNodes;
    while (nNodes)
    {
        // Input.
        cin >> nEdges >> source >> destination;
        vector<Edge> edges(nEdges);
        for (Edge &edge : edges)
            cin >> edge.source >> edge.destination >> edge.length;
        // Find the best paths and remove their edges from the graph.
        removeEdges(edges, getBestPaths(edges, source).second, destination);
        // Find the best path of the resulting graph.
        int res = getBestPaths(edges, source).first[destination];
        cout << (res == MAX_LENGTH ? -1 : res) << endl;
        cin >> nNodes;
    }

    return 0;
}
