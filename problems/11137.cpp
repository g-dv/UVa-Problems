/**
 * Problem : UVa n°11137
 * Name    : Ingenuous Cubrency
 * Author  : https://gitlab.com/g-dv/
 * Date    : 28/02/2019
 */

#include <iostream>
#include <vector>

using namespace std;

typedef unsigned int uint;

const uint MAX_AMOUNT = 10000;
const uint MAX_COIN_CUBE_ROOT = 21;

int main()
{
    // Initialize coins.
    vector<uint> coins;
    for (uint i = 1; i <= MAX_COIN_CUBE_ROOT; i++)
        coins.push_back(i * i * i);

    // Dynamic programming (coin change)
    vector<long long> nWays(MAX_AMOUNT, 0);
    nWays[0] = 1;
    for (uint i = 0; i < coins.size(); i++)
        for (uint j = coins[i]; j < MAX_AMOUNT; j++)
            nWays[j] += nWays[j - coins[i]];

    // Input-Output
    uint amount;
    while (cin >> amount)
        cout << nWays[amount] << endl;

    return 0;
}
