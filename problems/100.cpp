/**
 * Problem : UVa n°100
 * Name    : The 3n + 1 problem
 * Author  : https://gitlab.com/g-dv/
 * Date    : 04/09/2016
 */

#include <cstdio>
#include <algorithm>

using namespace std;

int main()
{
    int i, j, n;
    while(scanf("%d", &i) != EOF) {
        scanf("%d", &j);
        int maxCycleLength = 1;
        for(int k = min(i, j); k <= max(i, j); k++) {
            n = k;
            int cycleLength = 1;
            while(n != 1) {
                switch(n % 2) {
                case 1:
                    n = n * 3 + 1;
                    cycleLength++;
                    // Pas de break : n est devenu pair.
                case 0:
                    n >>= 1;
                    cycleLength++;
                }
            }
            if (cycleLength > maxCycleLength)
                maxCycleLength = cycleLength;
        }
        printf("%d %d %d\n", i, j, maxCycleLength);
    }
    
    return 0;
}