/**
 * Problem : UVa n°117
 * Name    : The Postal Worker Rings Once
 * Author  : https://gitlab.com/g-dv/
 * Date    : 19/10/2016
 */

#include <iostream>
#include <algorithm>
#include <vector>
#include <string>

using namespace std;

vector<char> nodes;
vector<int> nEdges;
vector<string> streets;
int nNodes;

/**
 * Calcule le chemin le plus court entre deux noeuds (Floyd-Warshall).
 * @param src  noeud d'origine
 * @param dest noeud de destination
 * @return la distance minimale entre les deux noeuds
 */
int shortestPath(int src, int dest)
{
    // On initialise la matrice à +INFINI.
    vector<vector<int>> dist (nNodes, vector<int>(nNodes, 1 << 29));
    
    // On met des 0 sur la diagonale.
    for(int i = 0; i < nNodes; i++)
        dist[i][i] = 0;
    
    // On indique les distances entre les noeuds d'une même rue.
    for(int i = 0; i < streets.size(); i++) { // pour chaque rue
        int length = streets[i].length();
        int j = 0, k = 0;
        while(j < nNodes && streets[i][0]          != nodes[j]) j++;
        while(k < nNodes && streets[i][length - 1] != nodes[k]) k++;
        dist[j][k] = dist[k][j] = length; // distance entre les deux noeuds
    }
    
    for(int k = 0; k < nNodes; k++)
        for(int i = 0; i < nNodes; i++)
            for(int j = 0; j < nNodes; j++)
                dist[i][j] = min(dist[i][j], dist[i][k] + dist[k][j]);
    
    return dist[src][dest];
}
/**
 * Ajoute un noeud dans la liste des noeuds s'il n'y est pas encore. Met
 * également à jour son nombre d'apparitions.
 * @param node char représentant le noeud à ajouter
 */
void addNode(char node)
{
    int i = 0;
    while(i < nNodes && node != nodes[i]) i++; // On cherche le noeud.
    if(i == nNodes) { // Le noeud n'existe pas encore. On ajoute le noeud.
        nNodes++;
        nodes.push_back(node);
        nEdges.push_back(1);
    } else // On incrémente le nombre de passage par le noeud.
        nEdges[i]++;   
}

int main()
{
    string street;
    while(cin >> street) {
        
        nNodes = 0;
        nEdges.clear();
        nodes.clear();
        streets.clear();
        
        int sumLength = 0; // Somme des longueurs des rues.
        
        do {
            // On ajoute la rue à la liste des rues (liste des arêtes).
            streets.push_back(street);
            
            // On met à jour le nombre de fois que chaque noeud a été utilisé.
            addNode(street[0]);
            addNode(street[street.size() - 1]);
               
            // On met à jour la longueur totale des rues.
            sumLength += street.length();
        } while(cin >> street, street != "deadend");
        
        // On compte le nombre de noeuds reliés à un nombre impair d'arêtes.
        // D'après Euler, si ce nombre est égal à 0 ou 2, une solution existe.
        // L'énoncé déclare qu'il y aura nécessairement 0 ou 2 noeuds avec un
        // nombre impair d'arêtes.
        vector<int> impairs;
        for(int i = 0; i < nNodes; i++)
            if((nEdges[i] % 2) == 1)
                impairs.push_back(i);

        // Si le nombre de noeuds avec un nombre impair d'arêtes vaut 0 alors on
        // peut faire un cycle complet qui passe par tous les noeuds du graphe.
        // C'est-à-dire que la solution va être simplement égale à sumLength.
        // En revanche, si le nombre de noeuds avec un nombre impair d'arêtes
        // vaut 2, alors cela signifie que le chemin le plus court qui fait un
        // tour complet en passant par tous les noeuds vaut : A + B avec :
        // A = chemin passant par tous les noeuds une seule fois = sumLength
        // B = chemin le plus court entre les deux noeuds ayant un nombre impair
        //     d'arêtes.
        if(impairs.size() == 2)
            sumLength += shortestPath(impairs[0], impairs[1]); // On ajoute B.
        
        cout << sumLength << endl;
    }

    return 0;
}