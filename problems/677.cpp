/**
 * Problem : UVa n°677
 * Name    : All Walks of length "n" from the first node
 * Author  : https://gitlab.com/g-dv/
 * Date    : 28/05/2017
 */

#include <iostream>
#include <vector>
#include <cstdio>

using namespace std;

vector<vector<int>> matrix;
int nNodes, pathLength, nPaths;

/**
 * Displays the path
 * @param path : list of nodes to display
 */
void printPath(vector<int> path)
{
    cout << '(';
    for (int i = 0; i < pathLength; i++) {
        if (i)
            cout << ",";
        cout << path[i];
    }
    cout << ')' << endl;
}

/**
 * Checks if a node is already visited
 * @param node : node to search for
 * @param path : already visited nodes
 * @return     : true if the node has already been visited
 */
bool findNode(int node, vector<int> path)
{
    for (int visited : path)
        if (visited == node)
            return true;
    return false;
}

/**
 * Recursive function.
 * Finds and displays all the possible paths of length n from the first node.
 * @param path : already visited nodes
 */
void backtrack(vector<int> path)
{
    // End condition
    if (path.size() == pathLength) {
        nPaths++;
        return printPath(path);
    }

    // Recursive call
    for (int node = 1; node <= nNodes; node++) {
        bool alreadyVisited = findNode(node, path);
        bool isAccessible = matrix[path.back() - 1][node - 1];
        if (isAccessible && !alreadyVisited) {
            vector<int> newPath(path);
            newPath.push_back(node);
            backtrack(newPath);
        }
    }
}

int main()
{
    while (true) {
        // Initialization
        nPaths = 0;
        matrix.clear();

        // Input.
        cin >> nNodes >> pathLength;
        pathLength++;

        for (int i = 0; i < nNodes; i++) {
            vector<int> empty;
            matrix.push_back(empty);
            for (int j = 0; j < nNodes; j++) {
                int state;
                cin >> state;
                matrix[i].push_back(state);
            }
        }

        // Processing.
        backtrack(vector<int>(1, 1));
        if(!nPaths)
            cout << "no walk of length " << pathLength - 1 << endl;

        // End.
        int separator;
        if(scanf("%d", &separator) == -1)
            break;
        else
            cout << endl;
    }

    return 0;
}

