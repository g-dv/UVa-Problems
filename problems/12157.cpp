/**
 * Problem : UVa n°12157
 * Name    : Tariff Plan
 * Author  : https://gitlab.com/g-dv/
 * Date    : 15/09/2017
 */

#include <iostream>

using namespace std;

int main() {
    int nTests;
    cin >> nTests;
    for (int j = 1; j <= nTests; j++) {

        // processing
        int nCalls;
        int juice = 0, mile = 0;
        cin >> nCalls;
        for (int i = 0; i < nCalls; i++) {
            int call;
            cin >> call;
            juice += (10 * (1 + (call / 30)));
            mile += (15 * (1 + (call / 60)));
        }

        // decision
        string bestPlan;
        int total;
        if (mile > juice) {
            bestPlan = "Mile";
            total = juice;
        } else if (juice > mile) {
            bestPlan = "Juice";
            total = mile;
        } else {
            bestPlan = "Mile Juice";
            total = mile;
        }

        // output
        cout << "Case " << j << ": " << bestPlan << " " << total << endl;
    }
    
    return 0;
}
