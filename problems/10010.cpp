/**
 * Problem : UVa n°10010
 * Name    : Where's Waldorf?
 * Author  : https://gitlab.com/g-dv/
 * Date    : 11/10/2016
 */

#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <cctype>

using namespace std;

/**
 * Classe représentant de simples coordonnées matricielles.
 */
class Coords {
public:
    Coords(int _i, int _j) : i(_i), j(_j) {};
    void operator+=(Coords coords) {
        i += coords.i;
        j += coords.j;
    };
    int i, j;
};

int main()
{
    // On initialise un vecteur comportant les 8 directions 
    // (NO, N, NE, O, E, SO, S, SE) sous forme de coordonnées.
    vector<Coords> directions;
    for(int i = -1; i <= 1; i++)
        for(int j = -1; j <= 1; j++)
            if(i != 0 || j != 0)
                directions.push_back(Coords(i, j));
    
    int nTests;
    cin >> nTests;
    while(nTests--) {
        
        int height, width;
        cin >> height >> width;
        
        vector<vector<char>> grid(height); // grille
        map<char, vector<Coords>> coords; // coordonnées des lettres
        
        // On lit la grille. On la stocke dans un tableau. On stocke les
        // coordonnées de chaque lettre dans un map. Cet indexation évitera de
        // lire tout le tableau quand on cherchera une lettre.
        char letter;
        for(int i = 0; i < height; i++) {
            cin.ignore();
            for(int j = 0; j < width; j++) {
                cin >> letter;
                letter = tolower(letter);
                if(coords.find(letter) == coords.end()) { // La lettre n'existe
                    vector<Coords> empty;                 // pas encore.
                    coords.insert(pair<char, vector<Coords>>(letter, empty));
                }
                coords[letter].push_back(Coords(i, j)); // ajout des coordonnées
                grid[i].push_back(letter); // remplissage de la grille
            }
        }
        
        // Lecture des mots.
        int nWords;
        cin >> nWords;
        
        for(int i = 0; i < nWords; i++) {
            // Saisie.
            string word;
            cin >> word;
            for(int j = 0; j < word.length(); j++)
                word[j] = tolower(word[j]);
            // Recherche.
            bool isFound = false;
            // Pour chaque position initiale possible.
            for(int j = 0; j < coords[word[0]].size() && !isFound; j++) {
                // On teste les huits directions.
                for(int k = 0; k < 8 && !isFound; k++) {
                    bool isValid = true;
                    // On enregistre la position initiale.
                    Coords first(coords[word[0]][j]), adjacent(first);
                    // On regarde si les autres lettres sont dans la direction.
                    for(int m = 1; m < word.length() && isValid; m++) {
                        adjacent += directions[k]; // On déplace notre index.
                        if(adjacent.i >= 0 && adjacent.i < height &&
                           adjacent.j >= 0 && adjacent.j < width)
                            // On regarde si la lettre correspond.
                            isValid = (word[m] == grid[adjacent.i][adjacent.j]);
                        else
                            isValid = false; // On sort de la grille.
                    }
                    if(isValid) { // Le mot a été trouvé.
                        isFound = true; // On sort de la boucle.
                        cout << first.i + 1 << " " << first.j + 1 << endl;
                    }
                }
            }
        }
        if(nTests)
            cout << endl;
    }

    return 0;
}
