/**
 * Problem : UVa n°11790
 * Name    : Murcia's Skyline
 * Author  : https://gitlab.com/g-dv/
 * Date    : 28/04/2017
 */

#include <vector>
#include <iostream>
#include <cmath>
#include <cstdio>

using namespace std;

struct Widths // cumulative width when...
{
    int inc = 0; // ...increasing
    int dec = 0; // ...decreasing
};

int main()
{
    int nTests;
    cin >> nTests;

    for (int testNo = 1; testNo <= nTests; testNo++) {
        // Input.
        int nBuildings;
        cin >> nBuildings;

        vector<int> heights(nBuildings);
        for (int i = 0; i < nBuildings; i++)
            cin >> heights[i];

        vector<Widths> widths(nBuildings);
        for (int i = 0; i < nBuildings; i++) {
            int width;
            cin >> width;
            widths[i].inc = width;
            widths[i].dec = width;
        }

        // Processing.
        // Bottom-Up dynamic programming
        // Main idea: We add buildings one by one to find the maximum cumulative
        // width that we can reach with them.
        Widths answer = widths[0];

        for (int i = 1; i < nBuildings; i++) {
            Widths bestPrevious;
            Widths& last = widths[i];

            for (int j = 0; j < i; j++) { // loop through previous buildings
                // We find the building that has the best cumulative width among
                // the previous buildings that are smaller than the last one.
                Widths previous = widths[j];

                if (heights[j] < heights[i] && previous.inc > bestPrevious.inc)
                    bestPrevious.inc = previous.inc;
                    
                if (heights[j] > heights[i] && previous.dec > bestPrevious.dec)
                    bestPrevious.dec = previous.dec;
            }
            // The last building's cumulative width is its width plus the best
            // previous building that fits. (The one we found just before.)
            last.inc += bestPrevious.inc;
            last.dec += bestPrevious.dec;
            
            // We update the answer if we found a better solution.
            answer.inc = max(answer.inc, last.inc);
            answer.dec = max(answer.dec, last.dec);
        }

        // Output.
        printf("Case %d. ", testNo);
        if (answer.inc >= answer.dec)
            printf("Increasing (%d). Decreasing (%d", answer.inc, answer.dec);
        else
            printf("Decreasing (%d). Increasing (%d", answer.dec, answer.inc);
        printf(").\n");
    }

    return 0;
}
