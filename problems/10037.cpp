/**
 * Problem : UVa n°10037
 * Name    : Bridge
 * Author  : https://gitlab.com/g-dv/
 * Date    : 26/09/2016
 */

#include <cstdio>
#include <vector>
#include <algorithm>

using namespace std;

const int MAX_PEOPLE_NUMBER = 1000;

enum Criterion { FASTEST, LONGEST };
int times[2][MAX_PEOPLE_NUMBER], nPeople[2], totalTime;
vector<vector<int>> steps; // mémorise les mouvements effectués.

/**
 * Transfert une personne d'un bord à l'autre.
 * @param src                : n° du bord d'origine
 * @param type               : critère de recherche (plus rapide ou plus lent)
 * @param increasesTotalTime : faut-il incrémenter le temps total
 */
void transfer(int src, Criterion type, bool increasesTotalTime)
{
    // Déclarations/Initialisations.
    int dest = (src + 1) % 2;
    int index, time = -1;
    if(type == FASTEST)
        time = 101;
    
    // Recherche de la personne cherchée.
    for(int i = 0; i < nPeople[src]; i++) {
        if((type == FASTEST && times[src][i] < time) ||
           (type == LONGEST && times[src][i] > time))
        {
            time = times[src][i];
            index = i;
        }
    }
    
    // Transfert.
    times[dest][nPeople[dest]] = times[src][index];
    nPeople[dest]++;
    nPeople[src]--;
    times[src][index] = times[src][nPeople[src]];
    
    // Incrémentation du temps total.
    if(increasesTotalTime)
        totalTime += time;
    
    // Mémorisation du mouvement dans "steps".
    steps[steps.size() - 1].push_back(time);
}

int main() {
	int nTests;
    scanf("%d", &nTests);
    while(nTests--) {
        
        // Saisie.
        scanf("%d", &nPeople[0]);
        nPeople[1] = 0;
        for(int i = 0; i < nPeople[0]; i++)
            scanf("%d", &times[0][i]);

        // Initialisation.
        totalTime = 0;
        steps.clear();
        vector<int> emptyVector;
        
        // Traitement. Deux algorithmes greedy.
        if(nPeople[0] > 1) {
            
            while(true) { // Toutes les personnes ne sont pas encores passées.
                
                // Les deux algorithmes greedy permettent de faire passer un
                // groupe de deux personnes (les plus lentes) et de ramener la
                // torche. Il faut choisir quel algorithme utiliser en
                // fonction des plus rapides et des plus lents.
                
                // On détermine les deux plus rapides et les deux plus lents.
                int fastest[2] = { 101, 101 }, longest[2] = { -1, -1 };
                for(int i = 0; i < nPeople[0]; i++) {
                    if(times[0][i] < fastest[1]) {
                        if(times[0][i] < fastest[0]) {
                            fastest[1] = fastest[0];
                            fastest[0] = times[0][i];
                        } else
                            fastest[1] = times[0][i];
                    }
                    if(times[0][i] > longest[1]) {
                        if(times[0][i] > longest[0]) {
                            longest[1] = longest[0];
                            longest[0] = times[0][i];
                        } else
                            longest[1] = times[0][i];
                    }
                }
    
                // On choisit l'algorithme à utiliser.
                
                // Algorithme 1 :
                // Idée générale : Faire passer les deux plus lents ensemble.
                // Méthode :
                // On fait passer les deux plus rapides
                // Le plus rapide retourne donner sa torche aux deux plus lents
                // On fait passer les deux plus lents
                // Le second plus rapide qui était resté de l'autre côté
                //      retourne chercher le plus rapide.
                
                // Algorithme 2 :
                // Idée générale : Faire faire des allers-retours au plus
                //      rapide pour qu'il fasse passer les deux plus lents
                //      un par un avec le plus rapide.
                // Méthode :
                // On fait passer le plus rapide et le plus lent.
                // Le plus rapide retourne chercher le deuxième plus lent.
                // Le deuxième plus lent passe avec le plus rapide.
                // Le plus rapide retourne de l'autre côté.
                
                // Décision :
                // Algorithme 1 si :
                //      (temps algo 2)  >= (temps algo 1)
                //  <=> (z + a + y + a) >= (b + a + z + b)
                //  <=> (a + y) >= (b + b) 
                if(longest[1] + fastest[0] >= fastest[1] + fastest[1]) {
                    
                    // 1er et 2e plus rapides ->
                    steps.push_back(emptyVector);
                    transfer(0, FASTEST, false);
                    transfer(0, FASTEST, true);
                    
                    if(nPeople[0] == 0) break; // Tout le monde est passé ?
                    
                    // 1er plus rapide <-
                    steps.push_back(emptyVector);
                    transfer(1, FASTEST, true);
                    
                    // 1er et 2e plus lents ->
                    steps.push_back(emptyVector);
                    transfer(0, LONGEST, true);
                    transfer(0, LONGEST, false);
                    
                    if(nPeople[0] == 0) break; // Tout le monde est passé ?
                    
                    // 2è plus rapide <-
                    steps.push_back(emptyVector);
                    transfer(1, FASTEST, true);
                    
                } else {
                    
                    // 1er plus rapide et 1er plus lent ->
                    steps.push_back(emptyVector);
                    transfer(0, FASTEST, false);
                    transfer(0, LONGEST, true);
                    
                    if(nPeople[0] == 0) break; // Tout le monde est passé ?
                    
                    // 1er plus rapide <-
                    steps.push_back(emptyVector);
                    transfer(1, FASTEST, true);
                    
                    // 1er plus rapide et 2e plus lent ->
                    steps.push_back(emptyVector);
                    transfer(0, FASTEST, false);
                    transfer(0, LONGEST, true);
                    
                    if(nPeople[0] == 0) break; // Tout le monde est passé ?
                    
                    // 1er plus rapide <-
                    steps.push_back(emptyVector);
                    transfer(1, FASTEST, true);
                    
                }
            }
        } else if(nPeople[0] == 1) { // S'il n'y avait qu'une personne.
            steps.push_back(emptyVector);
            totalTime = times[0][0];
            steps[0].push_back(times[0][0]);
        }
        
        // Sortie.
        printf("%d\n", totalTime);
        for(unsigned int i = 0; i < steps.size(); i++) {
            sort(steps[i].begin(), steps[i].end());
            for(unsigned int j = 0; j < steps[i].size(); j++) {
                if(j > 0)
                    printf(" ");
                printf("%d", steps[i][j]);
            }
            printf("\n");
        }
        if(nTests)
            printf("\n");
    }
	
	return 0;
}