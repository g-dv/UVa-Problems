/**: GDV
 * Problem : UVa n°1757
 * Name    : Secret Chamber at Mount Rushmore
 * Author  : https://gitlab.com/g-dv/
 * Date    : 15/02/2019
 */

#include <iostream>
#include <vector>
#include <map>
#include <utility>
#include <set>

using namespace std;

typedef unsigned int uint;
typedef char Node;
typedef pair<Node, Node> Edge;
typedef map<Node, map<Node, int>> DistanceMatrix;

const int ALPHABET_SIZE = 26;

/**
 * Uses modified Floyd-Warshall to compute the shortest paths between nodes.
 * @param nodes  vertices
 * @param edges  edges
 * @return distance matrix dist[src][dest]
 */
DistanceMatrix getDistances(const set<Node> &nodes, const vector<Edge> &edges)
{
    // Initialize matrix to +INFINITY.
    map<Node, int> tmp;
    for (int node : nodes)
        tmp[node] = ALPHABET_SIZE;
    DistanceMatrix dist;
    for (int node : nodes)
        dist[node] = tmp;

    // Set the edges we already have.
    for (const Edge &edge : edges)
        // set a distance of 1 because we don't care
        dist[edge.first][edge.second] = 1;

    // Find paths.
    for (int i : nodes)
        for (int j : nodes)
            for (int k : nodes)
                dist[j][k] = min(dist[j][k], dist[j][i] + dist[i][k]);

    return dist;
}

int main()
{
    int nEdges, nWords;
    while (cin >> nEdges)
    {
        // Input edges.
        cin >> nWords;
        set<Node> nodes;
        vector<Edge> edges(nEdges);
        for (Edge &edge : edges)
        {
            cin >> edge.first >> edge.second;
            nodes.insert(edge.first);
            nodes.insert(edge.second);
        }

        // Input words.
        vector<pair<string, string>> pairs(nWords);
        for (pair<string, string> &pair : pairs)
            cin >> pair.first >> pair.second;

        // Compute the shortest paths between nodes.
        DistanceMatrix dist = getDistances(nodes, edges);

        // Check the words.
        for (auto pair : pairs)
        {
            bool flag = false; // true if it's impossible
            if (pair.first.size() != pair.second.size())
                flag = true;
            for (int i = 0; i < pair.first.size() && !flag; i++)
            {
                char a = pair.first[i];
                char b = pair.second[i];
                flag = a != b && (dist[a][b] == ALPHABET_SIZE || !dist[a][b]);
            }
            cout << (flag ? "no" : "yes") << endl;
        }
    }

    return 0;
}
