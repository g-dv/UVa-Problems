/**
 * Problem : UVa n°10050
 * Name    : Hartals
 * Author  : https://gitlab.com/g-dv/
 * Date    : 26/09/2016
 */

#include <cstdio>

using namespace std;

const int MAX_PARTIES_NUMBER = 100;

int main() {
	int nTests;
    scanf("%d", &nTests);
    while(nTests--) {
        
        // Initialisation
        int parties[MAX_PARTIES_NUMBER];
        int nDays, nParties, nLostDays = 0;
        
        // Saisie.
        scanf("%d\n%d", &nDays, &nParties);
        for(int i = 0; i < nParties; i++)
            scanf("%d", &parties[i]);
        
        // Traitement.
        for(int i = 1; i <= nDays; i++) {
            bool isWeekEnd = (((i + 1) % 7) < 2);
            if(!isWeekEnd) {
                bool isHartal = false;
                for(int j = 0; j < nParties && !isHartal; j++)
                    isHartal = ((i % parties[j]) == 0);
                if(isHartal)
                    nLostDays++;
            }
        }
        
        // Sortie.
        printf("%d\n", nLostDays);
    }
	
	return 0;
}