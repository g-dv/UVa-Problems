/**
 * Problem : UVa n°118
 * Name    : Mutant Flatworld Explorers
 * Author  : https://gitlab.com/g-dv/
 * Date    : 18/10/2016
 */

#include <cstdio>
#include <iostream>
#include <string>

using namespace std;

bool grid[51][51]; // grille contenant l'emplacement des marqueurs
int maxX, maxY; // taille du terrain

/**
 * Cette classe représente de simples coordonnées (x, y).
 */
class Coords
{
    public:
    
    Coords()
    {
        this->x = 0;
        this->y = 0;
    };
    
    Coords(int x, int y)
    {
        this->x = x;
        this->y = y;
    };
    
    void operator+=(Coords coords)
    {
        x += coords.x;
        y += coords.y;
    };
    
    void operator-=(Coords coords)
    {
        x -= coords.x;
        y -= coords.y;
    };
    
    int x, y;
};

/**
 * Cette classe représente un robot.
 */
class Robot
{
    public:
    
    Robot(Coords position, char orientation)
    {
        isLost_ = false;
        position_ = Coords(position);
        switch(orientation) {
            case 'N': orientation_ = Coords(0, 1) ; break;
            case 'E': orientation_ = Coords(1, 0) ; break;
            case 'S': orientation_ = Coords(0, -1); break;
            case 'W': orientation_ = Coords(-1, 0);
        }
    };
    
    void moveForward()
    {
        position_ += orientation_;
        if(isOut()) { // Le robot sort du terrain.
            position_ -= orientation_; // On annule le mouvement.
            if(!grid[position_.x][position_.y]) // Il n'y avait pas de marqueur.
                isLost_ = grid[position_.x][position_.y] = true;
        }
    };
    
    void rotateLeft()
    {
        orientation_ = Coords(-orientation_.y, orientation_.x);
    };
    
    void rotateRight()
    {
        orientation_ = Coords(orientation_.y, -orientation_.x);
    };
    
    bool isOut()
    {
        return (
            position_.x > maxX || position_.y > maxY ||
            position_.x < 0    || position_.y < 0
        );
    };
    
    void print()
    {
        printf("%d %d ", position_.x, position_.y);
        
              if(orientation_.y == 1)   printf("N");
        else  if(orientation_.y == -1)  printf("S");
        else  if(orientation_.x == 1)   printf("E");
        else/*if(orientation_.x == -1)*/printf("W");

        if(isLost_)
            printf(" LOST");
        printf("\n");
    };

    bool isLost_;
    
    private:
    
    Coords position_;
    Coords orientation_;
};


int main()
{
    // Taille de la grille.
    scanf("%d %d", &maxX, &maxY);
    
    // Tant qu'on saisit des robots.
    int initialX, initialY;
    char orientation;
    while(scanf("%d %d %c", &initialX, &initialY, &orientation) == 3) {
        
        // Initialisation.
        Robot robot(Coords(initialX, initialY), orientation);
        
        // Saisie des opérations.
        string operations;
        cin >> operations;
        
        // On effectue les opérations. On s'arrête si le robot se perd.
        for(int i = 0; i < operations.length() && !robot.isLost_; i++) {
            switch(operations[i]) {
                case 'L': robot.rotateLeft() ; break;
                case 'R': robot.rotateRight(); break;
                case 'F': robot.moveForward();
            }
        }
        
        // Sortie.
        robot.print();
    }
    
    return 0;
}