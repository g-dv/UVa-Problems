/**
 * Problem : UVa n°12372
 * Name    : Packing for Holiday
 * Author  : https://gitlab.com/g-dv/
 * Date    : 26/09/2016
 */

#include <cstdio>

using namespace std;

int main() {
	int length, width, height, nTests, noTest = 0;
    scanf("%d", &nTests);
    while(nTests--) {
        // Saisie.
        scanf("%d %d %d", &length, &width, &height);
        
        // Traitement.
        noTest++;
        printf("Case %d: ", noTest);
        if(length <= 20 && width <= 20 && height <= 20)
            printf("good\n");
        else
            printf("bad\n");
    }
	
	return 0;
}