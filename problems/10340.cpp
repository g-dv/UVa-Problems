/**
 * Problem : UVa n°10340
 * Name    : All in All
 * Author  : https://gitlab.com/g-dv/
 * Date    : 24/09/2016
 */

#include <iostream>
#include <string>

using namespace std;

int main() {
	string entry, code;
    bool first = true;
	while(cin.good()) {
        if(!first || (first = false))
            cout << endl;
		cin >> entry >> code;
		bool isValid = true;
		for(unsigned int i = 0, j = 0; i < entry.size(); i++, j++) {
			while(entry[i] != code[j] && isValid) {
				if(j < code.size() - 1)
					j++;
				else
					isValid = false;
			}
		}
		cout << (isValid ? "Yes" : "No");
	}
	
	return 0;
}