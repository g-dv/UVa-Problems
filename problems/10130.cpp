/**
 * Problem : UVa n°10130
 * Name    : SuperSale
 * Author  : https://gitlab.com/g-dv/
 * Date    : 04/09/2016
 */

#include <algorithm>
#include <cstdio>

using namespace std;

int main()
{
    // Paramètres.
    const unsigned int MAX_OBJECT_NUMBER = 1000;
    const unsigned int MAX_OBJECT_WEIGHT = 30;
    const unsigned int MAX_PEOPLE_NUMBER = 100;
    
    // Initialisations.
    
    // optimalValue[i][j] Contient la somme maximale que peut obtenir une
    // personne capable de porter j kilos étant donnés i objets étudiés.
    // La dernière ligne du tableau contient donc la réponse pour chaque
    // capacité possible après avoir étudié tous les objets.
    // L'avant dernière ligne du tableau contient donc la réponse pour chaque
    // capacité possible sans tenir compte de l'existence du dernier objet.
    int optimalValue[1 + MAX_OBJECT_NUMBER][1 + MAX_OBJECT_WEIGHT];
    int objectsPrices[MAX_OBJECT_NUMBER], objectsWeights[MAX_OBJECT_NUMBER];
    int capacities[MAX_PEOPLE_NUMBER];
    int nTests, nObjects, nPeople;
    
    scanf("%d", &nTests);
    while(nTests--) {
        // Saisie des objets.
        scanf("%d", &nObjects);
        for(int i = 0; i < nObjects; i++) {
            scanf("%d %d", &objectsPrices[i + 1], &objectsWeights[i + 1]);
        }
        
        // Saisie des personnes.
        int maxCapacity = 0;
        scanf("%d", &nPeople);
        for(int i = 0; i < nPeople; i++) {
            scanf("%d", &capacities[i]);
            maxCapacity = max(capacities[i], maxCapacity);
        }
        
        // On remplit la première ligne (lorsqu'il n'y a aucun objet).
        for(int i = 0; i <= maxCapacity; i++) {
            optimalValue[0][i] = 0;
        }
        
        // On remplit la première colonne (lorsque la capacité est 0).
        for(int i = 0; i <= nObjects; i++) {
            optimalValue[i][0] = 0;
        }
        
        // On remplit le reste du tableau.
        for(int i = 1; i <= nObjects; i++) {
            for(int capacity = 1; capacity <= maxCapacity; capacity++) {
                if(capacity < objectsWeights[i]) {
                    // L'objet est trop lourd donc la valeur optimale ne change
                    // pas par rapport à la ligne précédente.
                    optimalValue[i][capacity] = optimalValue[i - 1][capacity];
                } else {
                    // L'objet pourrait être porté. On regarde si le prendre
                    // et optimiser l'espace restant à l'aide de la ligne
                    // précédente est plus optimal que de ne pas le prendre.
                    optimalValue[i][capacity] = max(
                        optimalValue[i - 1][capacity], // ne pas le prendre
                        objectsPrices[i]               // le prendre
                            + optimalValue[i - 1][capacity - objectsWeights[i]]
                    );
                }
            }
        }
        
        // On consulte la dernière ligne du tableau pour personne.
        int totalValue = 0;
        for(int i = 0; i < nPeople; i++) {
            totalValue += optimalValue[nObjects][capacities[i]];
        }
        
        // Sortie.
        printf("%d\n", totalValue);
    }
    
	return 0;
}