/**
 * Problem : UVa n°10189
 * Name    : Minesweeper
 * Author  : https://gitlab.com/g-dv/
 * Date    : 25/09/2016
 */

#include <iostream>

using namespace std;

const int MAX_HEIGHT = 450;
const int MAX_WIDTH = 100;

int main()
{
    int height, width;
    int count = 0;
	while(cin >> height >> width, (height || width)) {
        
        // Ignore le retour à la ligne.
        cin.ignore();
        
        // Saute une ligne entre les tests.
        if(count++)
            cout << endl;
        
        // Initialisation.
        char board[MAX_HEIGHT][MAX_WIDTH];
        for(int i = 0; i < height; i++)
            for(int j = 0; j < width; j++)
                board[i][j] = '0';      

        // Traitement.
        for(int i = 0; i < height; i++) {
            for(int j = 0; j < width; j++) {
                char character;
                cin >> character;
                if(character == '*') {
                    board[i][j] = '*';
                    for(int k = -1; k <= 1; k++)     // On incrémente les cases
                        for(int m = -1; m <= 1; m++) // adjacentes.
                            if((k || m) && (i + k >= 0) && (i + k < height)
                                       && (j + m >= 0) && (j + m < width))
                                if(board[i + k][j + m] != '*')
                                    board[i + k][j + m]++;
                }
            }
            cin.ignore(); // Passage à la ligne suivante.
        }
        
        // Sortie.
        cout << "Field #" << count << ":" << endl;
        for(int i = 0; i < height; i++) {
            for(int j = 0; j < width; j++)
                cout << board[i][j];
            cout << endl;
        }
    }

	return 0;
}