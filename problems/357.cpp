/**
 * Problem : UVa n°357
 * Name    : Let Me Count The Ways
 * Author  : https://gitlab.com/g-dv/
 * Date    : 17/09/2017
 */

#include <cstdio>
#include <algorithm>

using namespace std;

int main()
{
    // This problem is almost identical to p674.

    const int MAX_AMOUNT = 30000;
    const int COINS[5] = { 1, 5, 10, 25, 50 };
    
    unsigned long long nSolutions[MAX_AMOUNT + 1];
    fill_n(nSolutions, MAX_AMOUNT + 1, 1); // initialize with 1s
        
    for(int i = 1; i < 5; i++) // already filled for i = 0
        for(int j = COINS[i]; j <= MAX_AMOUNT; j++)
            nSolutions[j] += nSolutions[j - COINS[i]];

    // The array is filled for every possible value at once, so the execution
    // time is not proportional to the number of tests.
    int amount;
    while(scanf("%d", &amount) > 0)
        if (nSolutions[amount] == 1)
            printf("There is only 1 way to produce %d cents change.\n", amount);
        else
            printf("There are %llu ways to produce %d cents change.\n", nSolutions[amount], amount);

    return 0;
}
