/** 
 * Problem : UVa n°231
 * Name    : Testing the CATCHER
 * Author  : https://gitlab.com/g-dv/
 * Date    : 03/05/2017
 */

#include <vector>
#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    int testNo = 0;
    while (true) {
        // Input.
        vector<double> heights;
        int height;
        while (true) {
            cin >> height;
            if (height == -1) {
                if (!heights.size()) // second -1 in a row means exit
                    return 0;
                break;
            }
            heights.push_back(height);
        }

        // Processing. Method: Dynamic programming - LIS.
        // store the maximum score we can reach just after taking down missile i
        vector<int> cumulativeScores(1, 1);
        int answer = 0;
        for (int i = 1; i < heights.size(); i++) {
            int maxScore = 0;
            for (int j = 0; j < i; j++)
                if (heights[j] >= heights[i] && cumulativeScores[j] > maxScore)
                    maxScore = cumulativeScores[j];
            answer = max(answer, maxScore + 1);
            cumulativeScores.push_back(maxScore + 1);
        }
        
        // Output.
        if (testNo++)
            printf("\n");
        printf("Test #%d:\n", testNo);
        printf("  maximum possible interceptions: %d\n", answer);
    }
}

