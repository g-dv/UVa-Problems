/**
 * Problem : UVa n°120
 * Name    : Stacks of Flapjacks
 * Author  : https://gitlab.com/g-dv/
 * Date    : 14/10/2016
 */

#include <iostream>
#include <string>
#include <vector>
#include <sstream>

using namespace std;

int stack[31];
int size;

/**
 * Inverse l'ordre des éléments de l'array stack à partir d'une position 
 * comprise entre 1 et size.
 * @param position position à partir de laquelle on inverse
 */
void flip(int position)
{
    int file[30];
    // On met les éléments dans un autre array.
    for(int i = position; i <= size; i++)
        file[i - position] = stack[i];
    // On recharge les éléments à l'envers.
    for(int i = 0; i < size - position + 1; i++)
        stack[size - i] = file[i];
}

int main()
{
    string line;
    while(getline(cin, line)) {
        
        // Initialisation.
        int input;
        size = 0;
        istringstream stream(line);
        while(stream >> input)
            stack[++size] = input;
        
        // Affichage du tas.
        for(int i = 1; i <= size; i++) {
            cout << stack[i];
            if(i != size)
                cout << " ";
        }
        cout << endl;      
        
        // Inversions du vecteur pour les modifications.
        flip(1);
        
        
        // Pour chaque position.
        for(int i = 1; i < size; i++) {
            // On cherche le plus grand pancake.
            int maxWidth = -1, largest;
            for(int j = i; j <= size; j++) {
                if(stack[j] > maxWidth) {
                    largest = j;
                    maxWidth = stack[j];
                }
            }
            if(largest != i) { // Pas encore placé.
                // On met le plus grand pancake au sommet pour pouvoir le
                // glisser à la bonne place.
                if(largest != size) { // Pas encore au sommet.
                    cout << largest << " ";
                    flip(largest);
                }
                cout << i << " "; // On le glisse à sa place.
                flip(i);
            }
        }
        cout << 0 << endl;
    }
    
    return 0;
}