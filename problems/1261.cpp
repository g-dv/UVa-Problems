/**
 * Problem : UVa n°1261
 * Name    : String Popping
 * Author  : https://gitlab.com/g-dv/
 * Date    : 15/09/2017
 */

#include <iostream>
#include <utility>
#include <vector>
#include <algorithm>

using namespace std;

bool isPossible;
vector<string> visited;

/**
 * Recursive function. Tests all solutions.
 * @param input : word to test
 */
void backtrack(string input) {    

    // eliminate the words that have already been computed
    if (find(visited.begin(), visited.end(), input) != visited.end())
        return;
    else
        visited.push_back(input);

    // find the groups
    vector<pair<int, int>> groups;
    int count;
    for (int i = 0; i < input.size();) {
        count = 1;
        for (int j = i + 1; j < input.size() && input[j] == input[i]; j++)
            count++;
        if (count > 1)
            groups.push_back(make_pair(i, count));
        i += count;
    }

    // exit condition
    if (groups.empty()) {
        if (input.empty())
            isPossible = true;
        return;
    }   

    // recursive calls
    for (pair<int, int> group : groups) {
        string parameter = input;
        parameter.erase(group.first, group.second);
        backtrack(parameter);
    }
}

int main() {
    int nTests;
    cin >> nTests;
    while(nTests--) {

        // input
        string input;
        cin >> input;

        // processing
        isPossible = false;
        visited.clear();
        backtrack(input);

        // output
        cout << (isPossible ? 1 : 0) << endl;
    }

    return 0;
}
