/**
 * Problem : UVa n°1009
 * Name    : Baloons in a Box
 * Author  : https://gitlab.com/g-dv/
 * Date    : 14/02/2019
 */

#include <iostream>
#include <cmath>
#include <algorithm>
#include <cstdio>
#include <vector>
#include <cfloat>
#include <array>

using namespace std;

typedef unsigned int uint;
typedef array<int, 3> Point;

struct Baloon
{
    Baloon(const Point &tmpCenter, double tmpR)
    {
        r = tmpR;
        center = tmpCenter;
    }
    Point center;
    double r;
};

/**
 * Reads the input to set the coordinates of a point
 * @param point : point to populate
 */
void readPoint(Point &point)
{
    cin >> point[0] >> point[1] >> point[2];
}

/**
 * Computes the distance between two points
 * @param p1 : first point
 * @param p2 : second point
 * @return : distance between the points
 */
double getDistance(const Point &p1, const Point &p2)
{
    double distance = 0;
    for (uint i = 0; i < 3; i++)
        distance += pow((p1[i] - p2[i]), 2);
    return sqrt(distance);
}

/**
 * Tells if a point is inside the box defined by two opposite corners
 * @param point : point to check
 * @param corners : opposite corners of the box
 * @return : answer
 */
bool isInTheBox(const Point &point, const vector<Point> &corners)
{
    for (uint i = 0; i < 3; i++)
    {
        if (point[i] < corners[0][i] && point[i] < corners[1][i])
            return false;
        if (point[i] > corners[0][i] && point[i] > corners[1][i])
            return false;
    }
    return true;
}

/**
 * Returns the volume of the box
 * @param corners : opposite corners of the box
 * @return : volume of the box
 */
double getBoxVolume(const vector<Point> &corners)
{
    double volume = 1.0;
    for (uint i = 0; i < 3; i++)
        volume *= corners[0][i] - corners[1][i];
    return abs(volume);
}

/**
 * Computes the minimal distance between a point and the sides of the box
 * @param point : point
 * @param corners : opposite corners of the box
 * @return : minimal distance between the point and the sides
 */
double getMinDistanceToPlanes(const Point &point, const vector<Point> &corners)
{
    double minDistance = DBL_MAX;
    for (const Point &corner : corners)
    {
        for (uint i = 0; i < 3; i++)
        {
            double distanceToPlane = abs(point[i] - corner[i]);
            minDistance = min(minDistance, distanceToPlane);
        }
    }
    return minDistance;
}

/**
 * Computes the distance between a point and a baloon
 * @param point : point
 * @param corners : opposite corners of the box
 * @return : minimal distance between the point and the sides
 */
double getDistanceToBaloon(const Point &point, Baloon *baloon)
{
    // distance between centers
    double distance = getDistance(point, baloon->center);
    // distance between the point and the baloon
    return distance - baloon->r;
}

int main()
{
    uint noBox = 0;
    uint nPoints;
    cin >> nPoints;
    while (nPoints)
    {
        // Read the coordinates of the corners
        vector<Point> corners(2);
        readPoint(corners[0]);
        readPoint(corners[1]);
        // Read the coordinates of the points
        vector<Point> points;
        for (uint i = 0; i < nPoints; i++)
        {
            Point point;
            readPoint(point);
            if (isInTheBox(point, corners))
                points.push_back(point);
        }
        nPoints = points.size();
        sort(points.begin(), points.end()); // to use next permutation

        double maxVolume = 0.0;
        // We try all the permutations. (At most 6! = 720)
        do
        {
            vector<bool> availablePoints(nPoints, true);
            vector<Baloon> baloons;
            double volume = 0;
            for (uint i = 0; i < nPoints; i++)
            {
                // Check if the point is inside a baloon
                if (!availablePoints[i])
                    continue;

                // Initialize the new baloon
                baloons.push_back(Baloon(points[i], DBL_MAX));
                Baloon *newBaloon = &baloons.back();

                // Inflate the baloon : box limits
                Point center = newBaloon->center;
                double distance = getMinDistanceToPlanes(center, corners);
                newBaloon->r = min(newBaloon->r, distance);

                // Inflate the baloon : other baloons limits
                for (uint j = 0; j + 1 < baloons.size(); j++)
                {
                    distance = getDistanceToBaloon(center, &baloons[j]);
                    newBaloon->r = min(newBaloon->r, distance);
                }

                // Update the volume
                volume += (4.0 * M_PI / 3.0) * pow(newBaloon->r, 3.0);
                // Disable the points contained in the new baloon
                for (uint j = 0; j < nPoints; j++)
                    if (getDistance(points[i], points[j]) < newBaloon->r)
                        availablePoints[j] = false;
            }
            maxVolume = max(maxVolume, volume);
        } while (next_permutation(points.begin(), points.end()));

        uint result = round(getBoxVolume(corners) - maxVolume);
        printf("Box %u: %u\n\n", ++noBox, result);
        cin >> nPoints;
    }

    return 0;
}
