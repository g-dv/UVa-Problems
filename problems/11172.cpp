/**
 * Problem : UVa n°11172
 * Name    : Relational Operator
 * Author  : https://gitlab.com/g-dv/
 * Date    : 26/09/2016
 */

#include <cstdio>

using namespace std;

int main() {
	int nTests, a, b;
    scanf("%d", &nTests);
    while(nTests--) {
        
        // Saisie.
        scanf("%d %d", &a, &b);
        
        // Traitement.
        if(a < b)
            printf("<\n");
        else if(a > b)
            printf(">\n");
        else
            printf("=\n");
    }
	
	return 0;
}