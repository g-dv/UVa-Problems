# Tools

This directory contains various solutions to generic issues that I encountered while solving UVa problems.

These solutions are kept here, because they can be reused easily.
