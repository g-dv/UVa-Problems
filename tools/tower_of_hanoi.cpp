/*
 * Title       : Tower of Hanoi
 * Description : Solves the problem for n disks.
 * Author      : GDV
 * Date        : 05/09/2016
 */

#include <iostream>
#include <stack>
#include <cmath>

using namespace std;

stack<int> pegs[3];
int moveCount = 0;

/**
 * Affiche sur une ligne l'état des colonnes.
 * @description  pour chaque colonne indique le nombre de disques.
 *               N'indique pas la taille des disques.
 * @param  move  int indiquant le nombre de déplacements déjà effectués.
 */
void displayState(int move)
{
    printf("%3d |", move);
    for(int i = 0; i < 3; i++) {
        printf(" %d", (int)pegs[i].size());
    }
    printf("\n");
}

/**
 * Déplace un disque d'une colone à une autre puis affiche le nouvel état.
 * @param  src   int indiquant l'index de la colonne dont on enlève le disque.
 * @param  dest  int indiquant l'index de la colonne où on ajoute le disque.
 */
void move(int src, int dest)
{
    pegs[dest].push(pegs[src].top());
    pegs[src].pop();
    moveCount++;
    displayState(moveCount);
}

int main()
{
    // Entrée.
    int n;
    cout << "Number of disks : ";
    cin >> n;
    
    // On enfile les différents disques sur les piquets.
    // Le plus petit disque est numéroté "1".
    for(int i = 0; i < n; i++) {
        pegs[0].push(n - i);
    }
    
    // On désigne toujours par Z la colonne contenant le plus petit disque.
    int z = 0;
    
    // On détermine le sens de rotation du plus petit disque.
    // 1 si pair. -1 si impair. (pex. cas avec un seul disque.)
    int direction = (((n % 2) == 0) ? 1 : -1);
    
    // On calcule le nombre optimal de mouvements requis pour réussir, car
    // l'emploi d'une boucle for élimine tout risque de boucle infinie.
    int movements = pow(2, n) - 1;
    for(int i = 0; i < ((movements >> 1)); i++) {
        // On met à jour le numéro de la colonne contenant le plus petit
        // disque et les deux autres.
        int previousZ = z;
        z = (z + direction + 3) % 3;
        int a = (z + 1) % 3;
        int b = (z + 2) % 3;
        
        // On déplace le plus petit disque en fonction.
        move(previousZ, z);
        
        // On effectue ensuite un déplacement entre les deux autres colonnes A et B.
        if((pegs[a].size() != 0) && (pegs[b].size() != 0)) { // Colonnes non vides.
            // On déplace le plus petit sur le plus grand.
            if (pegs[a].top() > pegs[b].top())
                move(b, a);
            else
                move(a, b);
        } else if((pegs[a].size() == 0) && (pegs[b].size() != 0)) { // A vide.
            move(b, a);
        } else if((pegs[b].size() == 0) && (pegs[a].size() != 0)) { // B vide.
            move(a, b);
        }
    }
    // On déplace le plus petit disque en haut de la pile pour terminer l'algorithme.
    move(z, ((z + direction + 3) % 3));
    
    return 0;
}
